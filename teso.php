<?php
/*
$Bookmars = array(
   'https://discordapp.com/api/guilds/284453597704486913/widget.json',
   'https://discordapp.com/api/servers/284453597704486913/embed.json',
   'https://discordapp.com/api/v6/invite/4Y23mKU?with_counts=true',
   'https://discordapp.com/api/v6/users/284447705101631489/profile',
   'https://discordapp.com/api/v6/users/@me/devices',
   'https://discordapp.com/api/v6/channels/352358201825427456/messages',
   'https://discordapp.com/api/v6/guilds/284453597704486913/members?limit=50',
   'https://discordapp.com/api/users/@me/guilds',
   'https://discordapp.com/api/guilds/284453597704486913/channels',
   'https://discordapp.com/api/users/284447705101631489',
   'https://discordapp.com/api/users/@me/connections',
   'https://discordapp.com/api/users/@me',
   'https://discordapp.com/api/channels/352358201825427456/messages',
   'https://discordapp.com/api/v6/users/@me/guilds',
   'https://discordapp.com/api/guilds/284453597704486913/members/342087544126570497',
   'https://discordapp.com/api/guilds/284453597704486913',
   'https://discordapp.com/api/gateway', // <---!
   'https://discordapp.com/api/channels/384639417366872074/messages/465837621617098752',
   'https://discordapp.com/api/channels/384639417366872074/messages?limit=5',
   'https://discordapp.com/api/v6/voice/regions',
   'https://discordapp.com/api/channels/384639417366872074/messages',
   'https://discordapp.com/api/gateway/bot',
   'https://discordapp.com/api/channels/384639417366872074',
   'https://discordapp.com/api/channels/352358201825427456/messages/352359669861318666',
   'https://discordapp.com/api/channels/352358201825427456/messages/352359669861318666/reactions'

);
$ch = curl_init();
$f = fopen('request.txt', 'w+');
curl_setopt_array($ch, array(
    CURLOPT_URL            => $Bookmars[$_GET['id']], 
    CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_HTTPHEADER     => array('Authorization: Bot Mzk4MjMxODg2OTczMTA4MjI1.DkXssA.8pnezN18EYoHWJB8ynm8ZmChtuA'), 
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_FOLLOWLOCATION => 1,
    CURLOPT_VERBOSE        => 1,
    CURLOPT_SSL_VERIFYPEER => 0,	
    //CURLOPT_STDERR         => $f
));
$response = curl_exec($ch);
$encoded_result = json_encode(json_decode($response, True), JSON_PRETTY_PRINT); // just to put it in a pretty style.
echo curl_getinfo($ch)['url'] . ($encoded_result != NULL ? $encoded_result : ': Failure');
fwrite($f, ('[' .curl_getinfo($ch)['url']. ']: ' .($response != NULL ? $response : ': Failure'). '\r\n'));
fclose($f); curl_close($ch);
*/
include dirname(__FILE__) . "/DiscordWebhooks/GuildHandler.php";
$GuildHandler = new Discord\Guild($_GET['srvid'], 'Mzk4MjMxODg2OTczMTA4MjI1.DzJkhA.7j0FR1VRN-eQ1gBTg8vXsCtJ4qo');
$guild = $GuildHandler->Raw['guild'];
$data = array('invite' => $GuildHandler->Raw['data']['invite'], 'main' => $GuildHandler->Raw['data']['main']);

echo '<pre style="background-color:#ccc;font-family:Courier New,Courier,monospace;margin:12px;padding:5px;max-height:200px;overflow-y:scroll">
Guild Name: ' .$guild["name"]. ' or ' .$data['invite']["guild"]["name"]. '<br/>
Guild Members: ' .$data['invite']["approximate_presence_count"]. '/' .$data['invite']["approximate_member_count"]. '<br/>
Guild Invite Link: ' .$GuildHandler->RetriveInviteCode(False). '<br />
Guild Icon URL: ' .$GuildHandler->RetriveServerIconURL(). '<br />
Guild ID: ' .$data['invite']['guild']['id']. '<br />
Guild Region: ' .$GuildHandler->RetriveVoiceRegion($GuildHandler->RetriveGuildRegion())['name']. '<br />
Inviter: "Name" = ' .$GuildHandler->RetriveInviter()['name']. ' "Icon URL" = ' .$GuildHandler->RetriveInviter()['icon_url']. ' "Instant Invite" = ' .$GuildHandler->RetriveInviter()['instant_invite']. '<br />
Inviter(by inv-code): "Name" = ' .$GuildHandler->RetriveInviter('png', '4Y23mKU')['name']. ' "Icon URL" = ' .$GuildHandler->RetriveInviter('png', '4Y23mKU')['icon_url']. ' "Instant Invite" = ' .$GuildHandler->RetriveInviter('png', '4Y23mKU')['instant_invite'] . '<br />
Channel Messages Reading(by Channel ID&Message ID): "Channel ID" = ' .var_export($GuildHandler->RetriveChannelMessage($GuildHandler->RetriveWidgetChannel()['id'])). ' "Message ID" = ' .var_export($GuildHandler->RetriveChannelMessage($GuildHandler->RetriveWidgetChannel()['id'], '352359669861318666')) . '<br />
Channel Info Detecting(by Channel ID): ' .$GuildHandler->RetriveGuildChannels($GuildHandler->RetriveAFKChannel()['id'])['type'] . '<br />
Guild Role Info Detecting(by Role Name): ' .$GuildHandler->RetriveGuildRoleData('Administrator')['id'] . '<br />
Direct Guild Emoji Icon URL(by Emoji Name): ' .$GuildHandler->RetriveGuildEmoji('zombie') . '<br />
Guild Emoji Info Detecting(by Emoji Name): ' .$GuildHandler->RetriveGuildEmojiData('zombie')['id'] . '<br />
Guild Widget Info Detecting: "Enabled" = ' .boolval($GuildHandler->RetriveWidgetChannel()['enabled']). ' "ID" = ' .$GuildHandler->RetriveWidgetChannel()['id'] . ' "Name" = #' .$GuildHandler->RetriveGuildChannels($GuildHandler->RetriveWidgetChannel()['id'])['name']. '<br />
Guild Embed Info Detecting: "Enabled" = ' .boolval($GuildHandler->RetriveEmbedChannel()['enabled']). ' "ID" = ' .$GuildHandler->RetriveEmbedChannel()['id'] . ' "Name" = #' .$GuildHandler->RetriveGuildChannels($GuildHandler->RetriveEmbedChannel()['id'])['name']. '<br />
Guild AFK Channel Info Detecting: "ID" = ' .$GuildHandler->RetriveAFKChannel()['id']. ' "Name" = #' .$GuildHandler->RetriveGuildChannels($GuildHandler->RetriveAFKChannel()['id'])['name']. ' "Timeout" = ' .$GuildHandler->RetriveAFKChannel()['timeout'] . '<br />
Direct Guild Splash Icon URL: ' .$GuildHandler->RetriveGuildSplash()
. '</pre>';
/*var_dump($GuildHandler->RetriveGuildMember('342087544126570497'));
var_dump($GuildHandler->RetriveGuildOwner($data['main']['owner_id']));
var_dump($GuildHandler->RetriveBOTGuild());
var_dump($GuildHandler->LeaveBOTGuild());
var_dump($GuildHandler->RetriveBOTGuild('398168621664305152'));
var_dump($GuildHandler->DeleteInvCode($GuildHandler->RetriveInviteCode(True)));
var_dump($GuildHandler->CreateGuildChannel(array(
            'name' => 'Test-Hey',
            'type' => $GuildHandler->_CHANNEL_TYPES['GUILD_TEXT'],
            'parent_id' => '360840492264456202',
            'permission_overwrites' => array(
                array('deny'=> 871890001, 'type' => 'role', 'id' => '284453597704486913', 'allow' => 0),
                array('deny'=> 805306384, 'type' => 'role', 'id' => '345942310372245507', 'allow' => 0),
                array('deny'=> 0, 'type' => 'role', 'id' => '314778763143675905', 'allow' => 1049600),
            ),
            'nsfw' => False
)));
var_dump($GuildHandler->SetUserData(NULL, array(
           'username' => '[BR]Debug',
           'avatar' => 'https://visualstudio.microsoft.com/wp-content/uploads/2016/06/editing-your-code-1-562x309@2x-op.png'
)));
var_dump($GuildHandler->ModifyChannelPos(array(
           array('id' => '284458769386176512','position' => 6),
           array('id' => '352359756704251916','position' => 4)
)));
var_dump($GuildHandler->RetriveGuildMemberList());*/
/*var_dump($GuildHandler->CreateGuildRole(array(
           'name' => 'Debug',
           'permissions' => $GuildHandler->_Permissions_List['Administrator'][0],
           'color' => array('red' => 63, 'green' => 89, 'blue' => 119),
           'hoist' => False,
           'mentionable' => True
)));
var_dump($GuildHandler->RetriveGuildBans());
var_dump($GuildHandler->AddGuildMemberRole(array(
           'id' => '329012134937886730',
           'role' => '475416742760546335'
)));
var_dump($GuildHandler->DeleteGuildMemberRole(array(
           'id' => '329012134937886730',
           'role' => '475416742760546335'
)));
var_dump($GuildHandler->ModifyGuildMemberNick(array(
           'id' => '329012134937886730',
           'nick' => 'Unknown_Dude'
)));
var_dump($GuildHandler->DeleteGuildRole('475416742760546335'));
var_dump($GuildHandler->ModifyGuildRole(array(
    'name' => 'X-Debug',
    'permissions' => $GuildHandler->_Permissions_List['Administrator'][0],
    'color' => array('red' => 65, 'green' => 89, 'blue' => 119),
    'hoist' => False,
    'mentionable' => False
)));
var_dump($GuildHandler->BeginGuildPrune(array(
    'days' => 1
)));
var_dump($GuildHandler->RetriveGuildPruneCount());
var_dump($GuildHandler->RetriveGuildAuditLog($GuildHandler->_AuditLog_Objects['OBJECT_WEBHOOK']));
var_dump($GuildHandler->RetriveGuildAuditLog($GuildHandler->_AuditLog_Objects['OBJECT_USER']));
var_dump($GuildHandler->RetriveGuildAuditLog($GuildHandler->_AuditLog_Objects['OBJECT_ACTIVITY']));
var_dump($GuildHandler->RetriveGuildAuditLog(NULL));
var_dump($GuildHandler->CreateGuildEmoji(array(
    'name' => 'dudez',
    'image' => 'https://cdn.discordapp.com/emojis/283348081368367105.png',
    'roles' => array(
        array('deny'=> 871890001, 'type' => 'role', 'id' => '284453597704486913', 'allow' => 0),
        array('deny'=> 805306384, 'type' => 'role', 'id' => '345942310372245507', 'allow' => 0),
        array('deny'=> 0, 'type' => 'role', 'id' => '314778763143675905', 'allow' => 1049600),
    )
), True));
var_dump($GuildHandler->CreateGuildChannelMessage('284458769386176512', array(
    'content' => 'Hello, world! :alien:\ud83c\udf08',
    'tts' => False,
    'embed' => array(
            'title' => 'Test...',
            'type' => 'rich',
            'description' => 'Testoretoz',
            'color' => 11356937,
            'footer' => array(
                'text'   =>   'Hello footer',
                'icon_url'   =>   'https://vignette.wikia.nocookie.net/theamazingworldofgumball/images/a/af/Discord_Logo.png/revision/latest?cb=20170105233205'
            ),
            'image' => array('url' => 'https://png.icons8.com/ios/1600/discord-logo.png'),
            'thumbnail' => array('url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGmnrRD-rcF8bojIG4C-ZrDPqoR6EWJ630cLZdg-bmJJk5NwgjeQ'),
            'timestamp' => '2018-08-15T00:09:14.1534284554924+02:00',
            'author' => array(
                'name' => '[BR]John_Magdy',
                'url'   =>   'https://www.facebook.com/BR.Zoro',
                'icon_url'   =>   'https://graph.facebook.com/178950652664113/picture?type=small'
            ),
            'fields' => array(
                array(
                    'name' => 'Field Name #1',
                    'value' => 'Field Value #1',
                    'inline' => True
                ),
                array(
                    'name' => 'Field Name #2',
                    'value' => 'Field Value #2',
                    'inline' => True
                ),
                array(
                     'name' => 'Field Name #3',
                     'value' => 'Field Value #3',
                     'inline' => True
                ),
                array(
                    'name' => 'Field Name #4',
                     'value' => 'Field Value #4',
                    'inline' => True
               )
           )
    )
)));*/
var_dump($GuildHandler->CreateGuildEmoji(array(
    'name' => 'dudez',
    'image' => 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png',
    'roles' => array(
        array('deny'=> 871890001, 'type' => 'role', 'id' => '284453597704486913', 'allow' => 0),
        array('deny'=> 805306384, 'type' => 'role', 'id' => '345942310372245507', 'allow' => 0),
        array('deny'=> 0, 'type' => 'role', 'id' => '314778763143675905', 'allow' => 1049600),
    )
), False));
?>