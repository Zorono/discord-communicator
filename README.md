# Discord Communicator

| Travis CI | Total downloads | Latest release |
| :---: | :---: | :---: |
|  [![Build Status](https://travis-ci.org/JohnMagdy/discord-communicator.svg?branch=master)](https://travis-ci.org/JohnMagdy/discord-communicator)   |  [![All Releases](https://img.shields.io/github/downloads/JohnMagdy/discord-communicator/total.svg?maxAge=86400)](https://github.com/JohnMagdy/discord-communicator/releases)  |  [![latest release](https://img.shields.io/github/release/JohnMagdy/discord-communicator.svg?maxAge=86400)](https://github.com/JohnMagdy/discord-communicator/releases) <br> [![Github Releases](https://img.shields.io/github/downloads/JohnMagdy/discord-communicator/latest/total.svg?maxAge=86400)](https://github.com/JohnMagdy/discord-communicator/releases)  |  
-------------------------------------------------
Let's at first discuss some Basics...

**What is Discord ?**

Discord is a proprietary freeware voice-over-Internet Protocol application designed for gaming communities, that specializes in text and audio communication between users in a chat channel. [For more info...](https://en.wikipedia.org/wiki/Discord_(software))

**What is Discord Communicator ?**

[Discord Communicator](https://github.com/JohnMagdy/discord-communicator) is a simple lib which communicates with [Discord](https://discordapp.com)'s [webhooks](https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks) to send Messages to a specified Channel with JSON Content-Type through cURL. and its in background originally Created by [nopjmp](https://github.com/nopjmp) and it was called [discord-webhooks](https://github.com/nopjmp/discord-webhooks) and then its forked&developed by me since many months.

**Requirements:**

* PHP 5.6.36+
* cURL 7.59.0+
* Running Discord Server.
* Created Webhook in your Discord Server.

**Setting up a webhook in Discord:**

1. On your Discord Server open 'Server Settings' and select 'Webhooks' on the menu:
![https://i.imgur.com/AcZ6EZp.png](https://i.imgur.com/AcZ6EZp.png)
1. Click on the Button 'Create Webhook' and then complete the form ("Name", "avatar" [the fields's values that you are setting will be the defaults and it may be overriden in this wrapper, but the channel you pick cannot be changed by the wrapper.You'll need a separate URL for each channel you want to post to...]:
![https://i.imgur.com/rNwg31Z.png](https://i.imgur.com/rNwg31Z.png)
1. Copy the "Webhook URL" and use that when initializing the lib.


**What is the original Features of this lib and What is the new Features ?**
nopjmp's version's Features: 

* the Client/Embed class are less optimized.
* Supports most of the possible Embed/Messaging Data: tts/message/embed fields[description/color/author/title/image/thumbnail/timestamp/footer/fields]
* detects whether the color is hex or not and converts it to Decimal.
* and many more...

My Version's Features:

* Added the abillity to send your Data through cURL with JSON Content type...
* Improved the ERROR Handler Script.
* Optimized the Embed/Client Classes to be more cleaner.
* Made it so easy to send the Data using a small Array (User Friendly).
* Added Support for Hex colors and auto converstion from Hex Color to Decimel Color.
* Added Default Values for Timestamp and Color.
* Made most of the Data is optional.
* Added a list of usefull Decimal Colors.
* Added a kind of Advanced Embed Validator.
* It now Checks for Messages Length before sending the Data to Discord's Systems.
* and Alot more you can find it by yourself...

**How to send a simple Message to my Discord Server's Channel using this lib ?**
Using the Embed/Client Class directly:

1. We have to include the Class code to our project file:

```
include dirname(__FILE__) . "/DiscordWebhooks/Client.php";
include dirname(__FILE__) . "/DiscordWebhooks/Embed.php";
```
2. We initializate the $webhook var and then assign the Client Class and set the arguemnt(s) to your {DISCORD_WEBHOOK_URL}:`$webhook = new Discord/Client('https://discordapp.com/api/webhooks/{YOUR_CHANNEL_ID}/{YOUR_AUTH_TOKEN}');`
3. We initializate the $embed var and then assign the Embed Class without any arguments:`$embed = new Discord/Embed();`
4. Let's do the Magic: 

```
$embed->description('This is an embed'); // it sets the description value of your embed...
$webhook->username('Bot')->message('Hello, Human!')->embed($embed)->send(); // Let's set the Username of the BOT and send the Message
```

Using the cURL Method: Feel free to checkout the [cURL-Example.php](https://github.com/JohnMagdy/discord-communicator/blob/master/curl-exmp.php)

**List of Avaible Functions/Variables:**
```
Client Class:
public function tts($tts) // Controls the Message's tts settingswhether to be turned off/on (Boolean value, false by default)
public function username($username) // Controls the BOT's Username (String value, NULL by default)
public function avatar($new_avatar) // Controls the BOT's Avatar (String value, NULL by default)
public function message($new_message) // Controls the Message which will be sent to the requested Channel (String Value, NULL by default, limit: 2000chr)
public function embed($embed) // It Converts the Embed data to array
public function send() // It sends the requested Embed/Message data to the Discord Server and handles the errors

Embed Class:
public $COLOR_ // It Contains a list of usefull Decimal Colors (Array)
public function title($title, $url = '') // Controls the Title/Title URL of the Embed (String Value, NULL by default)
public function description($description) // Controls the description of the Embed (String Value, NUL by default)
public function timestamp($timestamp) // Controls the Embed's timestamp with the following Format: YYYY-MM-DDTHH:MM:SS.MSSZ (String Value, NULL by default)
public function color($color) // Controls the Embed's Side Color (Integer value, NULL by default, Decimal Colors only)
public function url($url) // Controls the Embed's master URL (String value, NULL by default)
public function footer($text, $icon_url = '') // Control the Embed's Footer's Text/Icon URL (String value, NULL by default)
public function image($url) // Controls the Embed's Side Image (String value, NULL by default)
public function thumbnail($url) // Controls the Embed's Thumbnail (String value, NULL by default)
public function author($name, $url = '', $icon_url = '') // Controls the Embed's Author Name/Author URL/Icon URL (String value, NULL by default)
public function field($name, $value, $inline = True) // Controls the Embed's Fields Name/Value/Inline (String/Boolean Value, NULL by default, up to 25 Fields)
public function toArray() // It Converts Embed Data to Array
```

**Default Decimal Color List ($COLOR_ var):**
    
```        
          'default'   =>   0,
          'aqua'   =>   1752220,
          'green'   =>   3066993,
          'blue'   =>   3447003,
          'purple'   =>   10181046,
          'gold'   =>   15844367,
          'orange'   =>   15105570,
          'red'   =>   15158332,
          'grey'   =>   9807270,
          'navy'   =>   3426654,
          'cyan'   =>   1277892,
          'brown'   =>   11356937,
          'dark_navy'   =>   2899536,
          'dark_grey'   =>   9936031,  
          'dark_aqua'   =>   1146986,
          'dark_green'   =>   2067276,
          'dark_blue'   =>   2123412,
          'dark_purple'   =>   7419530,
          'dark_gold'   =>   12745742,
          'dark_orange'   =>   11027200,
          'dark_red'   =>   10038562,
          'light_grey'   =>   12370112
```

## License

The project is MIT licensed. To read the full license, open [LICENSE.md](LICENSE.md).

## Contributing

Pull requests and issues are open!

<center><b>Looking around for any kind of Screenshots ? Here you are <a href="https://github.com/JohnMagdy/discord-communicator/tree/master/media"><img src="http://www.webofonie.com/wp-content/uploads/2016/05/ScreenshotFolderIcon.png" width="30" height="30" /></a></b></center>
