<?php
namespace Discord;
ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);
ini_set('memory_limit', '250M');
set_time_limit(0);
error_reporting(E_ALL);
class Guild {
	public $Raw = array();
	protected $mVars = array('ch' => array());
	protected $_ERR_CODES = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing...',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended'
	);
	public $_CHANNEL_TYPES = array(
		'GUILD_TEXT' => 0,
		'DM' => 1,
		'GUILD_VOICE' => 2,
		'GROUP_DM' => 3,
		'GUILD_CATEGORY' => 4
	);
    public $_Permissions_List = array(
		'Administrator' => array(8, 0x8),
		'ViewAuditLog' => array(128, 0x80),
		'ManageServer' => array(32, 0x20),
		'ManageRoles' => array(268435456, 0x10000000),
		'ManageChannels' => array(16, 0x10),
		'KickMembers' => array(2, 0x2),
		'BanMembers' => array(4, 0x4),
		'CreateInstantInvite' => array(1, 0x1),
		'ChangeNickname' => array(67108864, 0x4000000),
		'ManageNicknames' => array(134217728, 0x8000000),
		'ManageEmojis' => array(1073741824, 0x40000000),
		'ManageWebhooks' => array(536870912, 0x20000000),
		'ReadMessages' => array(1024, 0x400),
		'ViewChannel' => array(1024, 0x400),
		'SendTTSMessages' => array(4096, 0x1000),
		'EmbedLinks' => array(16384, 0x4000),
		'ReadMessageHistory' => array(65536, 0x10000),
		'UseExternalEmojis' => array(262144, 0x40000),
		'SendMessages' => array(2048, 0x800),
		'ManageMessages' => array(8192, 0x2000),
		'AttachFiles' => array(32768, 0x8000),
		'Mentioneveryone' => array(131072, 0x20000),
		'AddReactions' => array(64, 0x40),
		'Connect' => array(1048576, 0x100000),
		'MuteMembers' => array(4194304, 0x400000),
		'MoveMembers' => array(16777216, 0x1000000),
		'Speak' => array(2097152, 0x200000),
		'DeafenMembers' => array(8388608, 0x800000),
		'UseVoiceActivity' => array(33554432, 0x2000000),
		'PrioritySpeaker' => array(256, 0x100)
	);
    public $_AuditLog_Events = array(
		'GUILD_UPDATE' => 1,
		'CHANNEL_CREATE' => 10,
		'CHANNEL_UPDATE' => 11,
		'CHANNEL_DELETE' => 12,
		'CHANNEL_OVERWRITE_CREATE' => 13,
		'CHANNEL_OVERWRITE_UPDATE' => 14,
		'CHANNEL_OVERWRITE_DELETE' => 15,
		'MEMBER_KICK' => 20,
		'MEMBER_PRUNE' => 21,
		'MEMBER_BAN_ADD' => 22,
		'MEMBER_BAN_REMOVE' => 23,
		'MEMBER_UPDATE' => 24,
		'MEMBER_ROLE_UPDATE' => 25,
		'ROLE_CREATE' => 30,
		'ROLE_UPDATE' => 31,
		'ROLE_DELETE' => 32,
		'INVITE_CREATE' => 40,
		'INVITE_UPDATE' => 41,
		'INVITE_DELETE' => 42,
		'WEBHOOK_CREATE' => 50,
		'WEBHOOK_UPDATE' => 51,
		'WEBHOOK_DELETE' => 53,
		'EMOJI_CREATE' => 60,
		'EMOJI_UPDATE' => 61,
		'EMOJI_DELETE' => 62,
		'MESSAGE_DELETE' => 72
	);
    public $_AuditLog_Objects = array(
		'OBJECT_WEBHOOK' => 'webhooks',
		'OBJECT_USER' => 'users',
		'OBJECT_ACTIVITY' => 'audit_log_entries'
	);

	public function __construct($srvid, $bottoken)
	{
	   $this->mVars = array('srvid' => $srvid,
							'bottoken' => $bottoken
						   );
	   if(count($this->RetriveBOTGuild($srvid)) < 1)
	   {
	      throw new \Exception('[Discord-API]: The server returned an error while fetching the requested data: { "code": 0, "message": "401: Unauthorized", "error": "BOT Must be Guild Member" }');
	   }
	   $this->RetriveWidgetData();
	   $this->RetriveServerData($this->RetriveInviteCode());
	   $this->RetriveGuildData();
	}
	  
	protected function ValidateJSONResult($result)
	{
	   $_ERRORS = array(
		         'Unknown Invite', 'Invalid API version', '404: Not Found', '401: Unauthorized', 
				 '405: Method Not Allowed', '403: Forbidden', 'Missing Access', 'Unknown User', 
				 'Unknown Member', 'Bots cannot use this endpoint', 'Length Required'
	   );
	   $err_ = False;
	   $decoded_result = json_decode($result, True);
	   for($err = 0; $err < count($_ERRORS); $err++)
	   {
		  if(isset($decoded_result['message']))
		  {
			 if(strpos($decoded_result['message'], $_ERRORS[$err]))
			 {
			    $err_ = True;
			 }
		  }
		  else
		  {
		     break;
		  }
	   }
	   return $err_;
	}

	protected function RetriveWidgetData()
    {
	   $this->mVars['ch']['widget'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/widget.json");
	   curl_setopt_array($this->mVars['ch']['widget'], array(
             CURLOPT_CUSTOMREQUEST => 'GET',
			 CURLOPT_CONNECTTIMEOUT => 10,
			 CURLOPT_CONNECTTIMEOUT_MS => 1500,
			 CURLOPT_FORBID_REUSE => 1,
			 CURLOPT_FRESH_CONNECT => 1,
			 CURLOPT_TIMEOUT => 30,
			 CURLOPT_RETURNTRANSFER => 1,
			 CURLOPT_HTTPHEADER => ['Access-Control-Allow-Origin: *'],
			 CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
	   ));
       $outp = curl_exec($this->mVars['ch']['widget']);   

       if($errno = curl_errno($this->mVars['ch']['widget'])) 
       {
		  switch(curl_strerror($errno))
		  {
			 case 'Couldn\'t resolve host name': {
				throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['widget'])['url'], 0, 60, "..."));
			 }
			 break;
			 default: {
				//throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				$outp = curl_exec($this->mVars['ch']['widget']);
			 }
		  }		  
       }
       $_HTTP_CODE = curl_getinfo($this->mVars['ch']['widget'], CURLINFO_HTTP_CODE);	   
	   switch($_HTTP_CODE)
	   {
	      case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
	         if($this->ValidateJSONResult($outp) == False)
	         {
                $this->Raw['guild'] = json_decode($outp, True);
				if(json_last_error() != JSON_ERROR_NONE)
				{
				   switch (json_last_error()) 
				   {
   			          case JSON_ERROR_DEPTH:
    			         $json_err = 'Maximum stack depth exceeded';
   			             break;
    			      case JSON_ERROR_STATE_MISMATCH:
    			         $json_err = 'Underflow or the modes mismatch';
    			         break;
   			          case JSON_ERROR_CTRL_CHAR:
    			         $json_err = 'Unexpected control character found';
   			             break;
    		          case JSON_ERROR_SYNTAX:
       			         $json_err = 'Syntax error, malformed JSON';
   			             break;
   			          case JSON_ERROR_UTF8:
           		         $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                         break;
        			  default:
           		         $json_err = 'Unknown error';
        		         break;
    			   }
				   $error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
                   throw new \Exception($error);				   
				}
	         }
	         else
	         {
	            throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
	         }
		  }
		  break;
		  default: {
		     throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['widget'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['widget'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['widget'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		  }
		  break;
	   }
       $this->CloseDataConnection();
	}
	
	protected function RetriveServerData($inv_code)
	{
	   $this->mVars['ch']['data']['invite'] = curl_init("https://discordapp.com/api/v6/invite/" .((isset(parse_url($inv_code)['host']) ? parse_url($inv_code)['host'] == 'discord.gg' || parse_url($inv_code)['host'] == 'discordapp.com' : False) ? $this->SplitInviteCode($inv_code) : $inv_code). "?with_counts=true");
	   curl_setopt_array($this->mVars['ch']['data']['invite'], array(
             CURLOPT_CUSTOMREQUEST => 'GET',
			 CURLOPT_CONNECTTIMEOUT => 10,
			 CURLOPT_CONNECTTIMEOUT_MS => 1500,
			 CURLOPT_FORBID_REUSE => 1,
			 CURLOPT_FRESH_CONNECT => 1,
			 CURLOPT_TIMEOUT => 30,
			 CURLOPT_RETURNTRANSFER => 1,
			 CURLOPT_HTTPHEADER => ['Access-Control-Allow-Origin: *'],
			 CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
	   ));
       $outp = curl_exec($this->mVars['ch']['data']['invite']);   

       if($errno = curl_errno($this->mVars['ch']['data']['invite'])) 
       {
		  switch(curl_strerror($errno))
		  {
			 case 'Couldn\'t resolve host name': {
				throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['invite'])['url'], 0, 60, "..."));
			 }
			 break;
			 default: {
				//throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				$outp = curl_exec($this->mVars['ch']['data']['invite']);
			 }
		  }		  
       }
       $_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE);	   
	   switch($_HTTP_CODE)
	   {
	      case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
	         if($this->ValidateJSONResult($outp) == False)
	         {
                $this->Raw['data']['invite'] = json_decode($outp, True);
				if(json_last_error() != JSON_ERROR_NONE)
				{
				   switch (json_last_error()) 
				   {
   			          case JSON_ERROR_DEPTH:
    			         $json_err = 'Maximum stack depth exceeded';
   			             break;
    			      case JSON_ERROR_STATE_MISMATCH:
    			         $json_err = 'Underflow or the modes mismatch';
    			         break;
   			          case JSON_ERROR_CTRL_CHAR:
    			         $json_err = 'Unexpected control character found';
   			             break;
    		          case JSON_ERROR_SYNTAX:
       			         $json_err = 'Syntax error, malformed JSON';
   			             break;
   			          case JSON_ERROR_UTF8:
           		         $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                         break;
        			  default:
           		         $json_err = 'Unknown error';
        		         break;
    			   }
				   $error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
                   throw new \Exception($error);				   
				}
	         }
	         else
	         {
	            throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
	         }
		  }
		  break;
		  default: {
		     throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		  }
		  break;
	   }
       $this->CloseDataConnection(1);
	}

	protected function RetriveGuildData()
	{
	   $this->mVars['ch']['data']['main'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']);
	   if(isset($_SERVER['HTTP_USER_AGENT']))
	   {
		  curl_setopt($this->mVars['ch']['data']['main'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	   }	   
	   curl_setopt_array($this->mVars['ch']['data']['main'], array(
             CURLOPT_CUSTOMREQUEST => 'GET',
			 CURLOPT_CONNECTTIMEOUT => 10,
			 CURLOPT_CONNECTTIMEOUT_MS => 1500,
			 CURLOPT_FORBID_REUSE => 1,
			 CURLOPT_FRESH_CONNECT => 1,
			 CURLOPT_TIMEOUT => 30,
			 CURLOPT_RETURNTRANSFER => 1,
			 CURLOPT_HTTPHEADER => [
				  'Access-Control-Allow-Origin: *',
				  'Authorization: Bot ' . $this->mVars['bottoken']
			 ],
			 CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
	   ));
       $outp = curl_exec($this->mVars['ch']['data']['main']);   

       if($errno = curl_errno($this->mVars['ch']['data']['main'])) 
       {
		  switch(curl_strerror($errno))
	      {
			 case 'Couldn\'t resolve host name': {
                throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['main'])['url'], 0, 60, "..."));
			 }
			 break;
			 default: {
				//throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
			    echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
			    $outp = curl_exec($this->mVars['ch']['data']['main']);
			 }
          }
       }
       $_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['main'], CURLINFO_HTTP_CODE);	   
	   switch($_HTTP_CODE)
	   {
	      case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
	         if($this->ValidateJSONResult($outp) == False)
	         {
                $this->Raw['data']['main'] = json_decode($outp, True);
				if(json_last_error() != JSON_ERROR_NONE)
				{
				   switch (json_last_error()) 
				   {
   			          case JSON_ERROR_DEPTH:
    			         $json_err = 'Maximum stack depth exceeded';
   			             break;
    			      case JSON_ERROR_STATE_MISMATCH:
    			         $json_err = 'Underflow or the modes mismatch';
    			         break;
   			          case JSON_ERROR_CTRL_CHAR:
    			         $json_err = 'Unexpected control character found';
   			             break;
    		          case JSON_ERROR_SYNTAX:
       			         $json_err = 'Syntax error, malformed JSON';
   			             break;
   			          case JSON_ERROR_UTF8:
           		         $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                         break;
        			  default:
           		         $json_err = 'Unknown error';
        		         break;
    			   }
				   $error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
                   throw new \Exception($error);				   
				}
	         }
	         else
	         {
	            throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
	         }
		  }
		  break;
		  default: {
		     throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['main'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['main'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['main'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		  }
		  break;
	   }
       $this->CloseDataConnection(2);
	}

    protected function CloseDataConnection($mode = 0)
	{
	   switch($mode)
	   {
          case 0: {
		     curl_close($this->mVars['ch']['widget']);
			 break;
		  }
		  case 1: {
			 curl_close($this->mVars['ch']['data']['invite']);
			 break;
		  }
		  case 2: {
			curl_close($this->mVars['ch']['data']['main']);
			break;
		  }
		  case 3: {
			curl_close($this->mVars['ch']['data']['channel']);
			break;
		  }
		  case 4: {
			//curl_close($this->mVars['ch']['data']['gateway']);
			break;
		  }
		  case 5: {
			curl_close($this->mVars['ch']['data']['vregion']);
			break;
		  }	  
		  case 6: {
			curl_close($this->mVars['ch']['data']['user']);
			break;
		  }
		  case 7: {
			curl_close($this->mVars['ch']['data']['member']);
			break;
		  }
		  case 8: {
			curl_close($this->mVars['ch']['data']['guild']);
			break;
		  }
		  default: {
			curl_close($this->mVars['ch']['data']['invite']);
			break;
		  }
	   }
	}
	
	public function RetriveServerIconURL($ico_type = 'png')
	{
		$server_icon = 'https://cdn.discordapp.com/icons/' . $this->Raw['data']['invite']['guild']['id'] . '/' . $this->Raw['data']['invite']['guild']['icon'] . '.' . $ico_type;
		
		return (isset($this->Raw['data']['invite']['guild']['id']) && isset($this->Raw['data']['invite']['guild']['icon']) ? (file_get_contents($server_icon) ? $server_icon : False) : False); 
	}
	
	protected function SplitInviteCode($inv_code)
	{
	   $ds_inv_handlers = array('discordapp.com/invite/', 'discord.gg/');
	   
	   for($v = 0; $v < count($ds_inv_handlers); $v++)
	   {
	      if(strpos($inv_code, $ds_inv_handlers[$v]))
		  {
		     $inviteC = str_replace((isset(parse_url($inv_code)['scheme']) ? parse_url($inv_code, PHP_URL_SCHEME) . '://' : '') . $ds_inv_handlers[$v], '', $inv_code);
			 break;
		  }
		  else
		  {
		     continue;
		  }
	   }
	   return (isset($inviteC) ? $inviteC : NULL);
	}
	
	public function RetriveInviteCode($splitted = True)
	{
	   return (isset($this->Raw['guild']['instant_invite']) ? ($splitted ? $this->SplitInviteCode($this->Raw['guild']['instant_invite']) : $this->Raw['guild']['instant_invite']) : ($splitted ? '4Y23mKU' : 'discord.gg/4Y23mKU'));
	}

	public function RetriveInviter($ico_type = 'png', $inv_code = NULL, $json_encoded = False, $split_invcode = False)
	{
	   if($inv_code == NULL)
	   {
		  $_INVITE = (isset($this->Raw["data"]["invite"]["inviter"]) ? $this->Raw["data"]["invite"]["inviter"] : array('username' => NULL, 'discriminator' => NULL, 'id' => NULL, 'avatar' => NULL));
	      $_DATA = array('name' => (isset($this->Raw["data"]["invite"]["inviter"]) ? ($_INVITE["username"]. '#' .$_INVITE["discriminator"]) : NULL),
					     'icon_url' => (isset($this->Raw["data"]["invite"]["inviter"]) ? ('https://cdn.discordapp.com/avatars/' .$_INVITE["id"]. '/' .$_INVITE["avatar"]. '.' .$ico_type) : NULL),
					     'instant_invite' => $this->RetriveInviteCode($split_invcode)
		  );
	   }
	   else
	   {
		  $this->mVars['ch']['data']['invite'] = curl_init("https://discordapp.com/api/v6/invite/" .((isset(parse_url($inv_code)['host']) ? parse_url($inv_code)['host'] == 'discord.gg' || parse_url($inv_code)['host'] == 'discordapp.com' : False) ? $this->SplitInviteCode($inv_code) : $inv_code). "?with_counts=true");
		  curl_setopt_array($this->mVars['ch']['data']['invite'], array(
			    CURLOPT_CUSTOMREQUEST => 'GET',
			    CURLOPT_CONNECTTIMEOUT => 10,
			    CURLOPT_CONNECTTIMEOUT_MS => 1500,
			    CURLOPT_FORBID_REUSE => 1,
			    CURLOPT_FRESH_CONNECT => 1,
			    CURLOPT_TIMEOUT => 30,
			    CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_HTTPHEADER => ['Access-Control-Allow-Origin: *'],
				CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		  ));
		  $outp = curl_exec($this->mVars['ch']['data']['invite']);   
 
		  if($errno = curl_errno($this->mVars['ch']['data']['invite'])) 
		  {
			 switch(curl_strerror($errno))
			 {
				case 'Couldn\'t resolve host name': {
				   throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['invite'])['url'], 0, 60, "..."));
				}
				break;
				default: {
				   //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				   echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				   $outp = curl_exec($this->mVars['ch']['data']['invite']);
				}
			 }			 
		  }
		  $_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE);	   
		  switch($_HTTP_CODE)
		  {
		     case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			    if($this->ValidateJSONResult($outp) == False)
			    {
				   $decoded_data = json_decode($outp, True);
				   $_INVITE = (isset($decoded_data["inviter"]) ? $decoded_data["inviter"] : array('username' => 'BOT', 'discriminator' => '0000', 'id' => '0206855829', 'avatar' => 'fvaqzufikm'));
				   $_DATA = array('name' => ($_INVITE["username"]. "#" .$_INVITE["discriminator"]),
				                  'icon_url' => ('https://cdn.discordapp.com/avatars/' .$_INVITE["id"]. '/' .$_INVITE["avatar"]. '.' .$ico_type),
				                  'instant_invite' => $split_invcode ? $inv_code : ('https://discordapp.com/invite/' .$this->SplitInviteCode($inv_code))
				   );
				   if(json_last_error() != JSON_ERROR_NONE)
				   {
					  switch (json_last_error()) 
					  {
						 case JSON_ERROR_DEPTH:
						     $json_err = 'Maximum stack depth exceeded';
							 break;
					     case JSON_ERROR_STATE_MISMATCH:
						     $json_err = 'Underflow or the modes mismatch';
						     break;
						 case JSON_ERROR_CTRL_CHAR:
						     $json_err = 'Unexpected control character found';
							 break;
					     case JSON_ERROR_SYNTAX:
							 $json_err = 'Syntax error, malformed JSON';
							 break;
						 case JSON_ERROR_UTF8:
							 $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						     break;
					     default:
							 $json_err = 'Unknown error';
						     break;
					  }
					  $error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					  throw new \Exception($error);				   
				   }
			    }
			    else
			    {
				   throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			    }
		     }
		     break;
		     default: {
		  	    throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		     }
		     break;
		  }
		  $this->CloseDataConnection(1);
	   } 
	   return ($json_encoded ? json_encode($_DATA) : $_DATA);
	}

	public function RetriveGuildRegion()
	{
	   return (isset($this->Raw['data']['main']['region']) ? $this->Raw['data']['main']['region'] : 'ds-unknown');
	}

	public function RetriveGuildSplash($ico_type = 'png')
	{
	   return ($this->Raw['data']['main']['splash'] != NULL ? ("https://cdn.discordapp.com/splashes/" . $this->Raw['data']['main']['id'] . '/' . $this->Raw['data']['main']['splash'] . "." . $ico_type) : 'Failure');
	}

	public function RetriveAFKChannel()
	{
	   return array('id' => (isset($this->Raw['data']['main']['afk_channel_id']) ? $this->Raw['data']['main']['afk_channel_id'] : NULL), 'timeout' => $this->Raw['data']['main']['afk_timeout']);
	}

	public function IsEmbedEnabled()
	{
	   return ($this->Raw['data']['main']['embed_enabled'] ? (True) : (False));
	}

	public function RetriveEmbedChannel()
	{
	   return array('enabled' => $this->IsEmbedEnabled(), 'id' => ($this->IsEmbedEnabled() ? $this->Raw['data']['main']['embed_channel_id'] : NULL));
	}

	public function IsWidgetEnabled()
	{
	   return ($this->Raw['data']['main']['widget_enabled'] ? (True) : (False));
	}

	public function RetriveWidgetChannel()
	{
	   return array('enabled' => $this->IsWidgetEnabled(), 'id' => ($this->IsWidgetEnabled() ? $this->Raw['data']['main']['widget_channel_id'] : NULL));
	}

	public function RetriveGuildEmojiData($emj_name)
	{
	   foreach($this->Raw['data']['main']['emojis'] as $emoji)
	   {
		  if($emoji['name'] == $emj_name) // to avoid creating alot of cURL requests `guilds/{guild.id}/emojis/{emoji.id}`... 
		  {
             $emj_data = array(
			   	   'id' =>  $emoji['id'],
				   'name' => $emoji['name'],
				   'roles' => $emoji['roles'],
				   'require_colons' => boolval($emoji['require_colons']),
				   'managed' => boolval($emoji['managed']),
				   'animated' => boolval($emoji['animated'])
			 );
		  }
	   }
	   return isset($emj_data) ? $emj_data : array();
	}

	public function RetriveGuildEmoji($emj_name, $ico_type = 'png')
	{
	   return "https://cdn.discordapp.com/emojis/" . $this->RetriveGuildEmojiData($emj_name)['id'] . "." . $ico_type;
	}

	public function RetriveGuildRoleData($role_name)
	{
	   foreach($this->Raw['data']['main']['roles'] as $role)
	   {
		  if($role['name'] == $role_name)
		  {
             $role_data = array(
			   	   'id' =>  $role['id'],
				   'name' => $role['name'],
				   'color' => $role['color'],
				   'hoist' => boolval($role['hoist']),
				   'position' => $role['position'],
				   'permissions' => $role['permissions'],
				   'managed' => boolval($role['managed']),
				   'mentionable' => boolval($role['mentionable'])
			 );
		  }
	   }
	   return isset($role_data) ? $role_data : array();
	}

	public function RetriveGuildChannels($ch_id)
	{
	    $this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $channel = json_decode($outp, True);
				 $gd_channels = array(
						'id' => $channel['id'],
						'type' => $channel['type'],
						'guild_id' => isset($channel['guild_id']) ? $channel['guild_id'] : NULL,
						'position' => isset($channel['position']) ? $channel['position']: NULL,
						'permission_overwrites' => isset($channel['permission_overwrites']) ? $channel['permission_overwrites'] : NULL,
						'name' => isset($channel['name']) ? $channel['name'] : NULL,
						'topic' => isset($channel['topic']) ? $channel['topic'] : NULL,
						'nsfw' => isset($channel['nsfw']) ? boolval($channel['nsfw']) : NULL,
						'last_message_id' => isset($channel['last_message_id']) ? $channel['last_message_id'] : NULL,
						'bitrate' => isset($channel['bitrate']) ? $channel['bitrate'] : NULL,
						'user_limit' => isset($channel['user_limit']) ? $channel['user_limit'] : NULL,
						'recipients' => isset($channel['recipients']) ? $channel['recipients'] : NULL,
						'icon' => isset($channel['icon']) ? $channel['icon'] : NULL,
						'owner_id' => isset($channel['owner_id']) ? $channel['owner_id'] : NULL,
						'application_id' => isset($channel['application_id']) ? $channel['application_id'] : NULL,
						'parent_id' => isset($channel['parent_id']) ? $channel['parent_id'] : NULL
				 );
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($gd_channels) ? $gd_channels : array();
	}

	public function RetriveChannelMessage($ch_id, $msg_id = NULL, $msg_limit = 50)
	{
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_id. '/messages' .($msg_id != NULL ? '/' . $msg_id : ($msg_limit != NULL && $msg_limit > 0 ? '?limit=' . $msg_limit : '')));
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
		return isset($result) ? $result : json_encode(array());
	}

	public function RetriveVoiceRegion($region_id)
	{
		$this->mVars['ch']['data']['vregion'] = curl_init("https://discordapp.com/api/voice/regions");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['vregion'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['vregion'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['vregion']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['vregion'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['vregion'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['vregion']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['vregion'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 foreach(json_decode($outp, True) as $region)
				 {
					if($region['id'] == $region_id)
					{
				       $result = array(
						   'id' => $region['id'],
						   'name' => $region['name'],
						   'sample' => array(
							   'hostname' => (isset($data['sample_hostname']) ? $region['sample_hostname'] : NULL), 
							   'port' => (isset($data['sample_port']) ? $region['sample_port'] : NULL)
						   ),
						   'custom' => boolval($region['custom']),
						   'vip' => boolval($region['vip']),
						   'optimal' => boolval($region['optimal']),
						   'deprecated' => boolval($region['deprecated'])
					   );
					}
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['vregion'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['vregion'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['vregion'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(5);
		return isset($result) ? $result : array();
	}

	protected function RetriveGuildUser($member_id)
	{
		$this->mVars['ch']['data']['user'] = curl_init("https://discordapp.com/api/users/" .$member_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['user'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['user'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['user']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['user'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['user'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['user']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 $user = json_decode($outp, True);
				 if($this->ValidateJSONResult($outp) == False)
				 {
				    $result = array(
						'id' => $user['id'],
						'username' => $user['username'],
						'discriminator' => $user['discriminator'],
						'avatar' => $user['avatar'],
						'verified' => isset($user['verified']) ? boolval($user['verified']) : NULL,
						'mfa_enabled' => isset($user['mfa_enabled']) ? boolval($user['mfa_enabled']) : NULL,
						'email' => isset($user['email']) ? $user['email'] : NULL,
						'locale' => isset($user['locale']) ? $user['locale'] : NULL,
						'bot' => isset($user['bot']) ? boolval($user['bot']) : NULL,
						'flags' => isset($user['flags']) ? $user['flags'] : NULL
					);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error())
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(6);
		return isset($result) ? $result : array();
	}

	public function RetriveGuildMember($member_id)
	{
		$this->mVars['ch']['data']['member'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/members/" .$member_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['member'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['member'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['member']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['member'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['member'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['member']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 $member = json_decode($outp, True);
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$user_field = $this->RetriveGuildUser($member_id);
				    $result = array(
						'user' => array(
							'username' => $member['user']['username'],
							'discriminator' => $member['user']['discriminator'],
							'nick' => isset($member['user']['nick']) ? $member['user']['nick'] : NULL,
							'bot' => isset($member['user']['bot']) ? boolval($member['user']['bot']) : NULL,
							'avatar' => $member['user']['avatar']
						),						
						'roles' => $member['roles'],
						'joined_at' => $member['joined_at'],
						'deaf' => boolval($member['deaf']),
						'muted' => boolval($member['mute']),
						'email' => isset($user_field['email']) ? $user_field['email'] : NULL,
						'verified' => isset($user_field['verified']) ? $user_field['verified'] : NULL,
						'mfa_enabled' => isset($user_field['mfa_enabled']) ? boolval($user_field['mfa_enabled']) : NULL,
						'flags' => isset($user_field['flags']) ? $user_field['flags'] : NULL,
						'locale' => isset($user_field['locale']) ? $user_field['locale'] : NULL
					);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(7);
		return isset($result) ? $result : array();
	}

	public function RetriveGuildOwner($member_id)
	{
	   return (count($this->RetriveGuildMember($member_id)) >= 2 ? $this->RetriveGuildMember($member_id) : array());
	}

	public function LeaveBOTGuild()
	{	
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/users/@me/guilds/" .$this->mVars['srvid']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR')); 
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function RetriveGuildMemberList($limit = NULL)
	{
		$this->mVars['ch']['data']['member'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/members?limit=" .($limit != NULL && $limit >= 1 && $limit < 1000 ? $limit : $this->Raw['data']['invite']["approximate_member_count"]));
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['member'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['member'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['member']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['member'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['member'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['member']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 $list = json_decode($outp, True);
				 if($this->ValidateJSONResult($outp) == False)
				 {
					for($m = 0; $m < count($list); $m++)
					{
					   $user_field = $this->RetriveGuildUser($list[$m]['user']['id']);
				       $result[$m] = array(
						   'user' => array(
								 'username' => $list[$m]['user']['username'],
								 'discriminator' => $list[$m]['user']['discriminator'],
								 'nick' => isset($list[$m]['nick']) ? $list[$m]['nick'] : NULL,
								 'id' => $list[$m]['user']['id'],
								 'bot' => isset($list[$m]['user']['bot']) ? boolval($list[$m]['user']['bot']) : NULL,
								 'avatar' => $list[$m]['user']['avatar']
						   ),
						   'roles' => $list[$m]['roles'],
						   'joined_at' => $list[$m]['joined_at'],
						   'deaf' => boolval($list[$m]['deaf']),
						   'muted' => boolval($list[$m]['mute']),
						   'email' => isset($user_field['email']) ? $user_field['email'] : NULL,
						   'verified' => isset($user_field['verified']) ? $user_field['verified'] : NULL,
						   'mfa_enabled' => isset($user_field['mfa_enabled']) ? boolval($user_field['mfa_enabled']) : NULL,
						   'flags' => isset($user_field['flags']) ? $user_field['flags'] : NULL,
						   'locale' => isset($user_field['locale']) ? $user_field['locale'] : NULL
					   );
					}
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(7);
		return isset($result) ? $result : array();
	}

	public function RetriveBOTGuild($guild_id = NULL)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/users/@me/guilds");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [ 
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$guilds = json_decode($outp, True);
					for($g = 0; $g < count($guilds); $g++)
					{
					   if($guild_id == NULL)
					   {
				          $result[$g] = array(
						      'id' => $guilds[$g]['id'],
						      'name' => $guilds[$g]['name'],
						      'icon' => $guilds[$g]['icon'],
						      'owner' => boolval($guilds[$g]['owner']),
						      'permissions' => $guilds[$g]['permissions']
						  );
					   }
					   else
					   {
						  if($guilds[$g]['id'] == $guild_id)
						  {
						     $result = array(
							     'id' => $guilds[$g]['id'],
							     'name' => $guilds[$g]['name'],
							     'icon' => $guilds[$g]['icon'],
							     'owner' => boolval($guilds[$g]['owner']),
							     'permissions' => $guilds[$g]['permissions']
							 );
							 break;
						  }
					   }
					}
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function DeleteInvCode($inv_code)
	{
		$this->mVars['ch']['data']['invite'] = curl_init("https://discordapp.com/api/v6/invites/" .((isset(parse_url($inv_code)['host']) ? parse_url($inv_code)['host'] == 'discord.gg' || parse_url($inv_code)['host'] == 'discordapp.com' : False) ? $this->SplitInviteCode($inv_code) : $inv_code));
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['invite'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}
		curl_setopt_array($this->mVars['ch']['data']['invite'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['invite']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['invite'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['invite'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['invite']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['invite'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR')); 
		   }
		   break;
		}
		$this->CloseDataConnection(1);
		return isset($result) ? $result : array();
	}

	public function CreateGuildChannel(array $ch_data)
	{
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/channels");
		foreach($ch_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'name': {
		         if(strlen($value) > 2 && strlen($value <= 2000))
		         {
					if(preg_match('/[^a-zA-Z\d\-]/', $value))
			        {
					   if(strpos($value, ' '))
					   {
						  $ch_data = str_replace(' ', '-', $ch_data);
					   }
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.name` argument...(It couldn\'t contain special-characters, white-spaces)');
			        }
		         }
		         else
		         {
			        throw new \Exception('[Discord-API]: Invalid `data.guild.channel.name` argument\ length limit: ' .strlen($value). '/2000');
				 }
			 }
			 break;
			 case 'type': {
				 if(preg_match('/[^0-4]/', $value)) {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.type` argument...(Valid Types: 0-4)');
				 }
			 }
			 break;
			 case 'bitrate': {
				 if($ch_data['type'] == $this->_CHANNEL_TYPES['GUILD_VOICE'])
				 {
					if(is_numeric($value) == False)
					{
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.bitrate` argument...(Valid bitrate must be numeric[in bits])');
					}
				 }
				 else
				 {
				    throw new \Exception('[Discord-API]: the argument `data.guild.channel.bitrate` could only be used with the Voice Channels(Type: 2)');
				 }
			 }
			 break;
		     case 'user_limit': {
				 if($ch_data['type'] == $this->_CHANNEL_TYPES['GUILD_VOICE'])
				 {
					if(is_numeric($value) == False)
					{
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.user_limit` argument...(Valid user limit must be numeric)');
					}
				 }
				 else
				 {
				    throw new \Exception('[Discord-API]: the argument `data.guild.channel.user_limit` could only be used with the Voice Channels(Type: 2)');
				 }
			 }
			 break;
			 case 'permission_overwrites': {
				 if(is_array($value))
				 {
					for($p = 0; $p < count($value); $p++)
					{
					   foreach($value[$p] as $valuez)
					   {
					      switch($valuez)
					      {
					         case 'id': {
							    if(is_numeric($valuez) == False)
							    {
								   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission_overwrites.id` argument...(Valid ID must be numeric)');
							    }
						     }
						     break;
						     case 'type': {
							    if(is_string($valuez))
							    {
							       if($valuez != 'role' && $valuez != 'member')
							       {
								      throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission_overwrites.type` argument...(Valid Types: role, member)');	
							       }
							    }
							    else
							    {
								   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission_overwrites.type` argument...(Valid Type must be string[role, member])');
							    }
						     }
						     break;
						     case 'allow': {
						        if(is_numeric($valuez) == False)
							    {
							       throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission_overwrites.allow` argument...(Valid Allow-premission must be numeric[bitwise])');
							    }
						     }
						     break;
						     case 'deny': {
							    if(is_numeric($valuez) == False)
							    {
							       throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission_overwrites.deny` argument...(Valid Deny-premission must be numeric[bitwise])');
							    }
						     }
						     break;
						  }				  
					   }
					}
				 }
				 else
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission_overwrites` arguemnt...(Valid permission-overwrites must be array)');
				 }
			 }
			 break;
			 case 'parent_id': {
				if(is_numeric($value) == False)
				{
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.parent_id` arguemnt...(Valid category id must be numeric)');
				}
			 }
			 break;
			 case 'nsfw': {
				if(is_bool($value) == False)
				{
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.nsfw` arguemnt...(Valid legal-age-validation must be boolean)');
				}
			 }
			 break;
		   }
		}		
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($ch_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function SetUserData($user_id = NULL, array $data)
	{	
		$this->mVars['ch']['data']['user'] = curl_init("https://discordapp.com/api/users/" .($user_id == NULL ? '@me' : $user_id));
		foreach($data as $key => $value)
		{
		   switch($key)
		   {
		      case 'username': {
		         if(strlen($value) > 2 && strlen($value <= 32))
		         {
					if(preg_match('/[\@\#\:\`\s]/', $value))
			        {
				       throw new \Exception('[Discord-API]: Invalid Username: \'@\',\'#\',\':\',\'`\', whitespace');
			        }
		         }
		         else
		         {
			        throw new \Exception('[Discord-API]: Invalid `data.user.username` argument\ length limit: ' .strlen($value). '/32');
				 }
			 }
			 break;
			 case 'avatar': {
				 if(file_get_contents($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.user.avatar` argument...');
				 }
			 }
			 break;
		   }
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['user'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}
		curl_setopt_array($this->mVars['ch']['data']['user'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json', 
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['user']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['user'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['user'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['user']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
				    $result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['user'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR') . " (" .$outp. ")"); 
		   }
		   break;
		}
		$this->CloseDataConnection(6);
		return isset($result) ? $result : array();
	}

	public function ModifyChannelPos(array $ch_data)
	{
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/channels");
		foreach($ch_data as $value)
		{
		   foreach($value as $key => $valuez)
		   {
			  switch($key)
		      {
		         case 'id': {
				    if(is_numeric($valuez) == False)
				    {
				       throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid Channel ID must be numeric)');
				    }
			     }
			     break;
		         case 'position': {
				    if(is_numeric($valuez) == False)
				    {
				       throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.position` arguemnt...(Valid Channel position must be numeric)');
				    }
			     }
			     break;			  
			  }
		   }
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($ch_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function ModifyGuildMember($member_id, array $mem_data)
	{
		foreach($mem_data as $key => $value)
		{
		   switch($key)
		   {
			  case 'nick': {
				if(is_string($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.member.nick` arguemnt...(Valid member-nick must be string)');
				}
			  }
			  break;
			  case 'roles': {
				if(is_array($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.member.roles` arguemnt...(Valid member-roles must be array)');
				}
			  }
			  break;
			  case 'mute': {
				if(is_bool($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.member.mute` arguemnt...(Valid member-mute must be boolean)');
				}
			  }
			  break;
			  case 'deaf': {
				if(is_bool($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.member.deaf` arguemnt...(Valid member-deaf must be boolean)');
				}
			  }
			  break;
			  case 'channel_id': {
				if(is_numeric($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.member.channel_id` arguemnt...(Valid channel-id must be numeric)');
				}
			  }
			  break;			  			  			  			  
		   }
		}
		$this->mVars['ch']['data']['member'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/members/" .$member_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['member'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['member'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($mem_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['member']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['member'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['member'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['member']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(7);
		return isset($result) ? $result : array();
	}
	
	public function ModifyGuildMemberNick(array $mem_data)
	{
		if(is_string($mem_data['nick']) == False)
	    {
		   throw new \Exception('[Discord-API]: There must be a valid `data.member.nick` arguemnt...(Valid member-nick must be string)');
		}
		$this->mVars['ch']['data']['member'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/members/" .$mem_data['id']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['member'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['member'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode(array('nick' => $mem_data['nick'])),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['member']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['member'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['member'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['member']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(7);
		return isset($result) ? $result : array();
	}
	
	public function AddGuildMemberRole(array $mem_data)
	{
		foreach($mem_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'id':{
				 if(is_numeric($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.member.member_id` arguemnt...(Valid member-id must be numeric)');
				 }				 
			  }
			  break;
			  case 'role': {
				 if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.member.role_id` arguemnt...(Valid role-id must be numeric)');
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['member'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/members/" .$mem_data['id']. '/roles/' .$mem_data['role']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['member'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['member'], array(
			  CURLOPT_CUSTOMREQUEST => 'PUT',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['member']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['member'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['member'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['member']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(7);
		return isset($result) ? $result : array();
	}

	public function DeleteGuildMemberRole(array $mem_data)
	{
		foreach($mem_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'id':{
				 if(is_numeric($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.member.member_id` arguemnt...(Valid member-id must be numeric)');
				 }				 
			  }
			  break;
			  case 'role': {
				 if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.member.role_id` arguemnt...(Valid role-id must be numeric)');
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['member'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/members/" .$mem_data['id']. '/roles/' .$mem_data['role']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['member'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['member'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['member']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['member'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['member'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['member']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(7);
		return isset($result) ? $result : array();
	}

	public function DeleteGuildMember($mem_id)
	{
		if(is_numeric($mem_id) == False)
		{
		   throw new \Exception('[Discord-API]: There must be a valid `data.member.member_id` arguemnt...(Valid member-id must be numeric)');
		}
		$this->mVars['ch']['data']['member'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/members/" .$mem_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['member'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['member'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['member']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['member'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['member'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['member']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['member'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(7);
		return isset($result) ? $result : array();
	}

	public function RetriveGuildBans()
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/bans");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [ 
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$bans = json_decode($outp, True);
					if(count($bans) >= 1)
					{
					   foreach($bans as $ban)
					   {
						  for($b = 0; $b < count($bans); $b++)
						  {
					         $result[$b] = array(
						          'reason' => $bans[$b]['reason'],
						          'user' => $bans[$b]['user']
							 );
						  }
					   }
					}
					else
					{
						throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: {\"code\": 0, \"message\": \"404: Not Found\"}");
					}
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function BanGuildMember($mem_id, array $mem_data)
	{
		foreach($mem_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'reason': {
				 if(is_string($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.member.ban_reason` arguemnt...(Valid ban-reason must be string)');
				 }				 
			  }
			  break;
			  case 'delete-message-days': {
				if(is_numeric($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.member.delete_message_days` arguemnt...(Valid delete-message-days must be numeric)');
				}
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/bans/" .$mem_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'PUT',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($mem_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function DeleteGuildMemberBan($mem_id)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/bans/" .$mem_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function CreateGuildRole(array $role_data)
	{
		foreach($role_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'name': {
				 if(is_string($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_name` arguemnt...(Valid role-name must be string)');
				 }				 
			  }
			  break;
			  case 'permissions': {
				if(is_numeric($value) == False && ctype_xdigit($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_permissions` arguemnt...(Valid role-permissions must be numeric[decimal, hexadecimal, bitwise])');
				}
			  }
			  break;
			  case 'color': {
				if(is_array($value))
				{
					$c_arr = array('red','green','blue');
					for($c = 0; $c < count($c_arr); $c++)
					{
					   if(isset($role_data['color'][$c_arr[$c]]))
					   {
						  if($c == count($c_arr) -1)
						  {
							 $role_data['color'] = $role_data['color']['red'] * 16 ** 4 + $role_data['color']['green'] * 16 ** 2 + $role_data['color']['blue'];
							 break;
						  }
						  else
						  {
						     continue;
						  }
					   }
					   else
					   {
					      throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_color.' .$c_arr[$c]. '` arguemnt... (Valid role-color must be the \'the ' .$c_arr[$c]. ' value in the RGB color\')');
					   }
					}
				}
				else
				{
				   if($value == NULL)
				   {
				      $role_data['color'] = 0;
				   }
				   else
				   {
					  throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_color` arguemnt...(Valid role-color must be array[RGB color])');
				   }
				}
			  }
			  break;
			  case 'hoist': {
				if(is_bool($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_hoist` arguemnt...(Valid role-hoist must be boolean)');
				}
			  }
			  break;
			  case 'mentionable': {
				if(is_bool($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_mentionable` arguemnt...(Valid role-mentionable must be boolean)');
				}
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/roles");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($role_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function ModifyGuildRolePos(array $role_data)
	{
		foreach($role_data as $value)
		{
		   foreach($value as $key => $valuez)
		   {
			  switch($key)
		      {
		         case 'id': {
				    if(is_numeric($valuez) == False)
				    {
				       throw new \Exception('[Discord-API]: There must be a valid `data.role.id` arguemnt...(Valid Role ID must be numeric)');
				    }
			     }
			     break;
		         case 'position': {
				    if(is_numeric($valuez) == False)
				    {
				       throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.position` arguemnt...(Valid Role position must be numeric)');
				    }
			     }
			     break;			  
			  }
		   }
		}
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/roles");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($role_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function DeleteGuildRole($role_id)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/roles/" .$role_id);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function ModifyGuildRole(array $role_data)
	{
		foreach($role_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'name': {
				 if(is_string($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_name` arguemnt...(Valid role-name must be string)');
				 }				 
			  }
			  break;
			  case 'permissions': {
				if(is_numeric($value) == False && ctype_xdigit($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_permissions` arguemnt...(Valid role-permissions must be numeric[decimal, hexadecimal])');
				}
			  }
			  break;
			  case 'color': {
				if(is_array($value))
				{
					$c_arr = array('red','green','blue');
					for($c = 0; $c < count($c_arr); $c++)
					{
					   if(isset($role_data['color'][$c_arr[$c]]))
					   {
						  if($c == count($c_arr) -1)
						  {
							 $role_data['color'] = $role_data['color']['red'] * 16 ** 4 + $role_data['color']['green'] * 16 ** 2 + $role_data['color']['blue'];
							 break;
						  }
						  else
						  {
						     continue;
						  }
					   }
					   else
					   {
					      throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_color.' .$c_arr[$c]. '` arguemnt... (Valid role-color must be the \'the ' .$c_arr[$c]. ' value in the RGB color\')');
					   }
					}
				}
				else
				{
				   if($value == NULL)
				   {
				      $role_data['color'] = 0;
				   }
				   else
				   {
					  throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_color` arguemnt...(Valid role-color must be array[RGB color])');
				   }
				}
			  }
			  break;
			  case 'hoist': {
				if(is_bool($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_hoist` arguemnt...(Valid role-hoist must be boolean)');
				}
			  }
			  break;
			  case 'mentionable': {
				if(is_bool($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.role_mentionable` arguemnt...(Valid role-mentionable must be boolean)');
				}
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/roles");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($role_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function CreateGuildChannelMessage($ch_id, array $ch_data)
	{
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_id. "/messages");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($ch_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: ' .(isset($ch_data['file']) ? 'multipart/form-data' : 'application/json'),
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function BeginGuildPrune(array $gd_data)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/prune");
		if(is_numeric($gd_data['days']))
		{
		   if($gd_data['days'] < 1)
		   {
		      throw new \Exception('[Discord-API]: There must be a valid `data.guild.prune_days` arguemnt...(Valid Prune-days must be more than 1)');
		   }
		}
		else
		{
		   throw new \Exception('[Discord-API]: There must be a valid `data.guild.prune_days` arguemnt...(Valid Prune-days must be numeric[1 or more])');
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($gd_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
        return isset($result) ? $result : array();
	}

	public function RetriveGuildPruneCount()
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/prune");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function ModifyGuildEmbed(array $gd_data)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/embed");
		foreach($gd_data as $key => $field)
		{
		   switch($key)
		   {
		      case 'enabled': {
				 if(is_bool($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.embed.enabled` arguemnt...(Valid enabled must be boolean)');
				 }
			  }
			  break;
			  case 'channel_id': {
				 if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.embed.channel_id` arguemnt...(Valid channel-id must be numeric)');
				 }
			  }
			  break;
		   }
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($gd_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
        return isset($result) ? $result : array();
	}

	public function RetriveGuildAuditLog($object_type = 'audit_log_entries')
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/audit-logs");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$resrc = json_decode($outp, True);
					switch($object_type)
					{
					   case $this->_AuditLog_Objects['OBJECT_WEBHOOK']: {
					      $result = $resrc[$this->_AuditLog_Objects['OBJECT_WEBHOOK']];
					   }
					   break;
					   case $this->_AuditLog_Objects['OBJECT_USER']: {
					      $result = $resrc[$this->_AuditLog_Objects['OBJECT_USER']];
					   }
					   break;
					   case $this->_AuditLog_Objects['OBJECT_ACTIVITY']: {
					      $result = $resrc[$this->_AuditLog_Objects['OBJECT_ACTIVITY']];
					   }
					   break;
					   default: {
						  $result = $resrc;
						  //define('result', $resrc);
					   }
					   break;
					}
				 }
				 else
				 {
				    throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
		return isset($result) ? $result : array();
	}

	public function CreateGuildEmoji(array $gd_data, $encode_image = False)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/emojis");
		foreach($gd_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'name': {
				 if(is_string($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.name` arguemnt...(Valid name must be string)');
				 }
			  }
			  break;
			  case 'image': {
				 if($encode_image)
				 {
					$img_url = (preg_match('/\%[0-9A-F]{2}|\+/', $value) != False ? urldecode($value) : $value);
					if($img_content = file_get_contents($img_url))
					{
						if(strlen(mime_content_type($img_url)) > 2 ? strpos(mime_content_type($img_url), 'image/') != False : True)
						{
						   $finfo = new \finfo();
						   $img_mime = $finfo->file($img_url, FILEINFO_MIME_TYPE);
						   $gd_data['image'] = 'data:' .(isset($img_mime) && strlen($img_mime) > 2 ? $img_mime : "image/jpeg"). ';base64,' .base64_encode($img_content);
						}
						else
						{
							throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.image` arguemnt...(Valid image must be direct image url)');
						}
					}
					else
					{
						throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.image` arguemnt...(Valid image must be direct image url)');
					}
				 }
				 else
				 {
					$base64img = array();
					preg_match('/(data)\:(image)\/(.*)\;(base64)\,(.*)/', $value, $base64img); 
					$img = base64_decode($base64img[4], True);
					if(base64_encode($img) != $base64img[4])
				    {
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.image` arguemnt...(Valid image must be base64 encoded image)');
					}
				 }
			  }
			  break;
			  case 'roles': {
			     if(is_array($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.roles` arguemnt...(Valid roles must be array)');
				 }				 
			  }
			  break;
		   }
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($gd_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
        return isset($result) ? $result : array();
	}

	public function ModifyGuildEmoji(array $gd_data)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/emojis");
		foreach($gd_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'name': {
				 if(is_string($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.name` arguemnt...(Valid name must be string)');
				 }
			  }
			  break;
			  case 'roles': {
			     if(is_array($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.roles` arguemnt...(Valid roles must be array)');
				 }				 
			  }
			  break;
		   }
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($gd_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
        return isset($result) ? $result : array();
	}

	public function DeleteGuildEmoji($emoji_id)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']. "/emojis/" .$emoji_id);
		if(is_numeric($emoji_id) == False)
		{
		   throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.id` arguemnt...(Valid id must be numeric)');
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($gd_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
        return isset($result) ? $result : array();
	}

	public function ModifyGuild(array $gd_data)
	{
		$this->mVars['ch']['data']['guild'] = curl_init("https://discordapp.com/api/guilds/" .$this->mVars['srvid']);
		foreach($gd_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'name': {
				 if(is_string($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.name` arguemnt...(Valid name must be string)');
				 }
			  }
			  break;
			  case 'region': {
			     if(is_string($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.region` arguemnt...(Valid region must be string)');
				 }				 
			  }
			  break;
			  case 'verification_level': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.verification_level` arguemnt...(Valid verification-level must be numeric)');
				 }				 
			  }
			  break;
			  case 'default_message_notifications': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.default_message_notifications` arguemnt...(Valid default-message-notifications must be numeric)');
				 }				 
			  }
			  break;
			  case 'explicit_content_filter': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.explicit_content_filter` arguemnt...(Valid explicit-content-filter must be numeric)');
				 }				 
			  }
			  break;
			  case 'afk_channel_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.afk_channel_id` arguemnt...(Valid afk-channel-id must be numeric)');
				 }				 
			  }
			  break;
			  case 'afk_timeout': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.afk_timeout` arguemnt...(Valid afk-timeout must be numeric)');
				 }				 
			  }
			  break;
			  case 'icon': {
			     if(is_string($value))
				 {
					$base64img = array();
					preg_match('/(data)\:(image)\/(.*)\;(base64)\,(.*)/', $value, $base64img); 
					$img = base64_decode($base64img[4], True);
					if(base64_encode($img) != $base64img[4])
				    {
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.icon` arguemnt...(Valid icon must be base64 encoded image)');
					}
				 }
				 else
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.icon` arguemnt...(Valid icon must be string)');
				 }				 
			  }
			  break;
			  case 'owner_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.owner_id` arguemnt...(Valid owner-id must be numeric)');
				 }				 
			  }
			  break;
			  case 'splash': {
			     if(is_string($value))
				 {
					$base64img = array();
					preg_match('/(data)\:(image)\/(.*)\;(base64)\,(.*)/', $value, $base64img); 
					$img = base64_decode($base64img[4], True);
					if(base64_encode($img) != $base64img[4])
				    {
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.splash` arguemnt...(Valid splash must be base64 encoded image)');
					}
				 }
				 else
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.splash` arguemnt...(Valid splash must be string)');
				 }				 
			  }
			  break;
			  case 'system_channel_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.system_channel_id` arguemnt...(Valid system-channel-id must be numeric)');
				 }				 
			  }
			  break;
		   }
		}
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['guild'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['guild'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode($gd_data),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['guild']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['guild'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['guild'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['guild']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['guild'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(8);
        return isset($result) ? $result : array();
	}

	public function CreateMessageReaction(array $rec_data)
	{
		foreach($rec_data as $key => $value)
		{
		   switch($value)
		   {
		      case 'ch_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'emoji_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
		   }
		}
	    $this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$rec_data['ch_id']. '/messages/' .$rec_data['msg_id']. '/reactions/' .$rec_data['emoji_id']. '/@me');
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'PUT',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function DeleteMessageReaction(array $rec_data)
	{
		foreach($rec_data as $key => $value)
		{
		   switch($value)
		   {
		      case 'ch_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'emoji_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'user_id': {
			     if(is_numeric($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'del_all': {
			     if(is_bool($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.del_all` arguemnt...(Valid del-all must be boolean)');
				 }
			  }
			  break;
		   }
		}
	    $this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$rec_data['ch_id']. '/messages/' .$rec_data['msg_id']. '/reactions' .($rec_data['del_all'] ? ''  : '/' .$rec_data['emoji_id']. '/' .($rec_data['user_id'] == NULL && is_numeric($rec_data['user_id']) == False ? '@me' : $rec_data['user_id'])));
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function RetriveMessageReaction(array $rec_data)
	{
		foreach($rec_data as $key => $value)
		{
		   switch($value)
		   {
		      case 'ch_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'emoji_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.emoji.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
		   }
		}
		if(isset($rec_data['emoji_id']))
		{
		   $this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$rec_data['ch_id']. '/messages/' .$rec_data['msg_id']. '/reactions/' .$rec_data['emoji_id']);
		   if(isset($_SERVER['HTTP_USER_AGENT']))
		   {
			  curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		   }	   
		   curl_setopt_array($this->mVars['ch']['data']['channel'], array(
				 CURLOPT_CUSTOMREQUEST => 'GET',
				 CURLOPT_CONNECTTIMEOUT => 10,
				 CURLOPT_CONNECTTIMEOUT_MS => 1500,
				 CURLOPT_FORBID_REUSE => 1,
				 CURLOPT_FRESH_CONNECT => 1,
				 CURLOPT_TIMEOUT => 30,
				 CURLOPT_RETURNTRANSFER => 1,
				 CURLOPT_HTTPHEADER => [
					  'Access-Control-Allow-Origin: *',
					  'Authorization: Bot ' . $this->mVars['bottoken']
				 ],
				 CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		   ));
		   $outp = curl_exec($this->mVars['ch']['data']['channel']);   
	
		   if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		   {
			  switch(curl_strerror($errno))
			  {
				 case 'Couldn\'t resolve host name': {
					throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
				 }
				 break;
				 default: {
					//throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
					echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
					$outp = curl_exec($this->mVars['ch']['data']['channel']);
				 }
			  }
		   }
		   $_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		   switch($_HTTP_CODE)
		   {
			  case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
				 if($this->ValidateJSONResult($outp) == False)
				 {
					$result = json_decode($outp, True);
					if(json_last_error() != JSON_ERROR_NONE)
					{
					   switch (json_last_error()) 
					   {
						  case JSON_ERROR_DEPTH:
							 $json_err = 'Maximum stack depth exceeded';
							 break;
						  case JSON_ERROR_STATE_MISMATCH:
							 $json_err = 'Underflow or the modes mismatch';
							 break;
						  case JSON_ERROR_CTRL_CHAR:
							 $json_err = 'Unexpected control character found';
							 break;
						  case JSON_ERROR_SYNTAX:
							 $json_err = 'Syntax error, malformed JSON';
							 break;
						  case JSON_ERROR_UTF8:
							 $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
							 break;
						  default:
							 $json_err = 'Unknown error';
							 break;
					   }
					   $error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					   throw new \Exception($error);				   
					}
				 }
				 else
				 {
					throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
				 }
			  }
			  break;
			  default: {
				 throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
			  }
			  break;
		   }
		   $this->CloseDataConnection(3);
		}
		else
		{
		   $react = RetriveChannelMessage($rec_data['ch_id'], $rec_data['msg_id'])['reactions'];
		   for($r = 0; $r < count($react); $r++)
		   {
		      $result[$r] = array(
			      'count' => $react[$r]['count'],
			      'me' => boolval($react[$r]['me']),
			      'emoji' => $react[$r]['emoji']
		      );
		   }
		}
        return isset($result) ? $result : array();
	}

	public function ModifyChannelMessage(array $ch_data)
	{
		foreach($ch_data as $key => $value)
		{
		   switch($key)
		   {
			  case 'id': {
				 if(is_numeric($valuez) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_id': {
				 if(is_numeric($valuez) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_content': {
				 if(is_string($valuez))
				 {
					 if(strlen($value) > 2000)
					 {
						 throw new \Exception('[Discord-API]: `data.guild.channel.message.content` argument\'s length limit excceded: ' .strlen($value). '/2000');
					 }
				 }
				 else
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.content` arguemnt...(Valid Content must be string)');
				 }
			  }
			  break;
			  case 'msg_embed': {
				 if(is_array($valuez) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.embed` arguemnt...(Valid Embed must be array)');
				 }
			  }
			  break;		  
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_data['id']. "/messages/" .$ch_data['msg_id']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode(array('content' => $ch_data['content'], 'embed' => $ch_data['embed'])),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function DeleteChannelMessage(array $rec_data)
	{
		foreach($rec_data as $key => $value)
		{
		   switch($value)
		   {
		      case 'ch_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
		   }
		}
	    $this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$rec_data['ch_id']. '/messages/' .$rec_data['msg_id']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function BulkDeleteChannelMessage(array $rec_data)
	{
		foreach($rec_data as $key => $value)
		{
		   switch($value)
		   {
		      case 'ch_id': {
			     if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_count': {
			     if(is_numeric($value) == False || $value < 2 || $value > 100)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.msg_count.` arguemnt...(Valid msg-count must be numeric[2-100])');
				 }
			  }
			  break;
		   }
		}
	    $this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$rec_data['ch_id']. '/messages/bulk-delete');
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode(array('messages' => $rec_data['msg_count'])),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function ModifyChannelPermissions(array $perm_data)
	{
		foreach($perm_data as $key => $value)
		{
		   switch($key)
		   {
			  case 'id': {
				 if(is_numeric($valuez) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'role_id': {
				 if(is_numeric($valuez) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.role.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'permission_allow': {
				 if(is_numeric($valuez) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.role.permission.allow` arguemnt...(Valid permission-allow must be numeric[bitwise])');
				 }
			  }
			  break;
			  case 'permission_deny': {
				 if(is_numeric($valuez) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.role.permission.deny` arguemnt...(Valid permission-deny must be numeric[bitwise])');
				 }
			  }
			  break;
			  case 'permission_type': {
				 if(is_string($valuez))
				 {
					if($valuez != 'role' && $valuez != 'member')
					{
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission.type` argument...(Valid Types: role, member)');	
					}
				 }
				 else
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.permission.type` argument...(Valid Type must be string[role, member])');
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_data['id']. "/permissions/" .$ch_data['role_id']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'PUT',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode(array('allow' => $perm_data['permission_allow'], 'deny' => $perm_data['permission_deny'], 'type' => $perm_data['permission_type'])),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function RetrivePinnedChannelMessage($ch_id)
	{
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_id. '/pins');
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
		return isset($result) ? $result : json_encode(array());
	}

	public function PinChannelMessage(array $ch_data)
	{
		foreach($ch_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'id': {
				 if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_id': {
				 if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_data['id']. '/pins/' .$ch_data['msg_id']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'PUT',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
		return isset($result) ? $result : json_encode(array());
	}

	public function UnpinChannelMessage(array $ch_data)
	{
		foreach($ch_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'id': {
				 if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'msg_id': {
				 if(is_numeric($value) == False)
				 {
				    throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.message.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_data['id']. '/pins/' .$ch_data['msg_id']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
		return isset($result) ? $result : json_encode(array());
	}

	public function CreateGuildChannelWebhook(array $wh_data)
	{
		foreach($wh_data as $key => $value)
		{
		   switch($key)
		   {
			  case 'ch_id': {
				 if(is_numeric($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'name': {
				 if(is_string($value) == False || strlen($value) < 2 || strlen($value) > 32)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhook.name` argument...(Valid name must be string[2-32chr])');
				 }
			  }
			  break;
			  case 'avatar': {
				 if(is_string($value))
				 {
					$base64img = array();
					preg_match('/(data)\:(image)\/(.*)\;(base64)\,(.*)/', $value, $base64img); 
					$img = base64_decode($base64img[4], True);
					if(base64_encode($img) != $base64img[4])
				    {
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhook.avatar` arguemnt...(Valid avatar must be base64 encoded image)');
					}
				 }
				 else
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhook.avatar` argument...(Valid avatar must be string[role, member])');
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$wh_data['ch_id']. "/webhooks");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode(array('name' => $wh_data['name'], 'avatar' => $wh_data['avatar'])),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function RetriveGuildWebhooks(array $wh_data)
	{
		foreach($wh_data as $key => $value)
		{
		   switch($key)
		   {
		      case 'ret_all': {
				 if(is_bool($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhooks.ret_all` arguemnt...(Valid ret_all must be boolean)');
				 }
			  }
			  break;
			  case 'webhook': {
				 foreach($value as $keyz => $valuez)
				 {
					switch($key)
					{
					   case 'id': {
						  if(is_numeric($value) == False)
						  {
						     throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhooks.id` arguemnt...(Valid ID must be numeric)');
						  }
					   }
					   break;
					   case 'token': {
						  if(is_string($value) == False && $value != NULL)
						  {
						     throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhooks.token` arguemnt...(Valid token must be string)');
						  }
					   }
					   break;
					}
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/" .($wh_data['ret_all'] ? "guilds/" .$this->mVars['srvid']. "/webhooks" : "webhooks/" .$wh_data['webhook']['id']. ($wh_data['webhook']['token'] == NULL ? '' : "/" .$wh_data['webhook']['token'])));
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function RetriveGuildChannelWebhooks($ch_id)
	{
		if(is_numeric($ch_data) == False)
		{
		   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/channels/" .$ch_id. "/webhooks");
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function ModifyGuildWebhooks(array $wh_data)
	{
		foreach($wh_data as $key => $value)
		{
		   switch($key)
		   {
			  case 'ch_id': {
				 if(is_numeric($value) == False)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.id` arguemnt...(Valid ID must be numeric)');
				 }
			  }
			  break;
			  case 'name': {
				 if(is_string($value) == False || strlen($value) < 2 || strlen($value) > 32)
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhook.name` argument...(Valid name must be string[2-32chr])');
				 }
			  }
			  break;
			  case 'avatar': {
				 if(is_string($value))
				 {
					$base64img = array();
					preg_match('/(data)\:(image)\/(.*)\;(base64)\,(.*)/', $value, $base64img); 
					$img = base64_decode($base64img[4], True);
					if(base64_encode($img) != $base64img[4])
				    {
					   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhook.avatar` arguemnt...(Valid avatar must be base64 encoded image)');
					}
				 }
				 else
				 {
					throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhook.avatar` argument...(Valid avatar must be string[role, member])');
				 }
			  }
			  break;
			  case 'webhook': {
				 foreach($value as $keyz => $valuez)
				 {
					switch($key)
					{
					   case 'id': {
						  if(is_numeric($value) == False)
						  {
						     throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhooks.id` arguemnt...(Valid ID must be numeric)');
						  }
					   }
					   break;
					   case 'token': {
						  if(is_string($value) == False && $value != NULL)
						  {
						     throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhooks.token` arguemnt...(Valid token must be string)');
						  }
					   }
					   break;
					}
				 }
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/webhooks/" .$wh_data['webhook']['id']. ($wh_data['webhook']['token'] == NULL ? '' : "/" .$wh_data['webhook']['token']));
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_POSTFIELDS => json_encode(array('name' => $wh_data['name'], 'avatar' => $wh_data['avatar'], 'channel_id' => (isset($wh_data['ch_id']) ? $wh_data['ch_id'] : NULL))),
			  CURLOPT_HTTPHEADER => [
				   'Content-Type: application/json',
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}

	public function DeleteGuildWebhooks(array $wh_data)
	{
		foreach($wh_data as $key => $value)
		{
		   switch($key)
		   {
			  case 'id': {
				if(is_numeric($value) == False)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhooks.id` arguemnt...(Valid ID must be numeric)');
				}
			  }
			  break;
			  case 'token': {
				if(is_string($value) == False && $value != NULL)
				{
				   throw new \Exception('[Discord-API]: There must be a valid `data.guild.channel.webhooks.token` arguemnt...(Valid token must be string)');
				}
			  }
			  break;
		   }
		}
		$this->mVars['ch']['data']['channel'] = curl_init("https://discordapp.com/api/webhooks/" .$wh_data['id']. "/" .$wh_data['token']);
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
		   curl_setopt($this->mVars['ch']['data']['channel'], CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		}	   
		curl_setopt_array($this->mVars['ch']['data']['channel'], array(
			  CURLOPT_CUSTOMREQUEST => 'DELETE',
			  CURLOPT_CONNECTTIMEOUT => 10,
			  CURLOPT_CONNECTTIMEOUT_MS => 1500,
			  CURLOPT_FORBID_REUSE => 1,
			  CURLOPT_FRESH_CONNECT => 1,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_RETURNTRANSFER => 1,
			  CURLOPT_HTTPHEADER => [
				   'Access-Control-Allow-Origin: *',
				   'Authorization: Bot ' . $this->mVars['bottoken']
			  ],
			  CURLOPT_HTTP_VERSION => (explode('HTTP/', $_SERVER['SERVER_PROTOCOL'])[1] === 1.1 ? CURL_HTTP_VERSION_1_1 : CURL_HTTP_VERSION_1_0)
		));
		$outp = curl_exec($this->mVars['ch']['data']['channel']);   
 
		if($errno = curl_errno($this->mVars['ch']['data']['channel'])) 
		{
		   switch(curl_strerror($errno))
		   {
			  case 'Couldn\'t resolve host name': {
				 throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). ': ' .mb_strimwidth(curl_getinfo($this->mVars['ch']['data']['channel'])['url'], 0, 60, "..."));
			  }
			  break;
			  default: {
				 //throw new \Exception("[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno));
				 echo "[Discord-API]: cURL error (" .$errno. "): " .curl_strerror($errno). " Retrying...";
				 $outp = curl_exec($this->mVars['ch']['data']['channel']);
			  }
		   }
		}
		$_HTTP_CODE = curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE);	   
		switch($_HTTP_CODE)
		{
		   case ($_HTTP_CODE == 200 || $_HTTP_CODE == 201 || $_HTTP_CODE == 202 || $_HTTP_CODE == 204): {
			  if($this->ValidateJSONResult($outp) == False)
			  {
				 $result = json_decode($outp, True);
				 if(json_last_error() != JSON_ERROR_NONE)
				 {
					switch (json_last_error()) 
					{
					   case JSON_ERROR_DEPTH:
						  $json_err = 'Maximum stack depth exceeded';
						  break;
					   case JSON_ERROR_STATE_MISMATCH:
						  $json_err = 'Underflow or the modes mismatch';
						  break;
					   case JSON_ERROR_CTRL_CHAR:
						  $json_err = 'Unexpected control character found';
						  break;
					   case JSON_ERROR_SYNTAX:
						  $json_err = 'Syntax error, malformed JSON';
						  break;
					   case JSON_ERROR_UTF8:
						  $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						  break;
					   default:
						  $json_err = 'Unknown error';
						  break;
					}
					$error = '[Discord-API]: The server returned an error while fetching the requested data: ' .$json_err;
					throw new \Exception($error);				   
				 }
			  }
			  else
			  {
				 throw new \Exception("[Discord-API]: The server returned an error while fetching the requested data: " .$outp);
			  }
		   }
		   break;
		   default: {
			  throw new \Exception('[Discord-API]: ' . curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE) . ': ' . (isset($this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)]) ? $this->_ERR_CODES[curl_getinfo($this->mVars['ch']['data']['channel'], CURLINFO_HTTP_CODE)] : 'Unknown ERROR'));
		   }
		   break;
		}
		$this->CloseDataConnection(3);
        return isset($result) ? $result : array();
	}
}
?>