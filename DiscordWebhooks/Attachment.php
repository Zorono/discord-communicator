<?php
namespace Discord;

class Attachment
{

    protected $file;
    protected $name;
    protected $mime;

    public function __construct($path, $name = NULL) 
    {
        $rPath = realpath($path);
        $this->file = ($rPath != NULL && strlen($rPath) >= 2 ? $rPath : $path);
        $this->name = ($name == NULL ? basename($path) : $name);
        $this->mime = (strpos('https', $this->file) || strpos('http', $this->file) || strpos('www', $this->file)) ? NULL : mime_content_type($this->file);
        /*
        $header = @get_headers($this->file);
		for($h = 0; $h < count($header); $h++)
		{
		   if(strpos($header[$h], 'Content-Type'))
		   {
              $mytp = explode('Content-Type: ', $header[$h])[1];
              break;			 
		   }
		   else
           {
              if($header[$h] == $header[count($header)-1])
              {
                 $mytp = NULL;                  
                 break;
              }
              else
              {
                 continue;
              }
           }
		}
        $this->mime = ($mytp != NULL && strlen($mytp) >= 2 ? $mytp : NULL);
        */
    }
    public function getFile() 
    {
        return $this->file;
    }
    public function getName() 
    {
        return $this->name;
    }
    public function getMimeType()
    {
        return $this->mime;
    }
}
?>