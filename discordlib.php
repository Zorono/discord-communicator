<?php
   header('Content-Type: application/json;charset=utf-8');

   include dirname(__FILE__) . "/DiscordWebhooks/Client.php";
   include dirname(__FILE__) . "/DiscordWebhooks/Embed.php";
   include dirname(__FILE__) . "/DiscordWebhooks/Attachment.php";

   $_START_TIME = microtime(True);
   function DS_SendBOTMessage(array $data)
   {
	  $_embedC = 0;
	  $execute = True;
	  $_Namespaces = array();
	  
      if(isset($data['main']) && $data['main'] != NULL && is_array($data['main']))
	  {
	     foreach($data['main'] as $key => $value)
		 {
	        if($key == end($data['main']))
		    {
			   break;
			}
			switch($key)
			{
			   case ($key == 'channel_id' || $key == 'webhook_authtoken'):
			         if($value != NULL)
					 {
						$_Namespaces['webhook'] = new Discord\Client(('https://discordapp.com/api/webhooks/' . $data['main']['webhook_id'] . '/' . $data['main']['webhook_authtoken']));
						$_Namespaces['embed'] = new Discord\Embed();
					 }
					 else
					 {
						$execute = False;
					    throw new \Exception('[Discord-API]: There must be a valid Webhook ID');
					 }
			   break;
		       case 'bot_username':
			         if($value != NULL)
					 {
						if(strlen($value) > 2 && strlen($value) <= 32)
						{
						   if(strpos('@', $value) == False && strpos('#', $value) == False && strpos(':', $value) == False && strpos('`', $value) == False && strpos(' ', $value) == False)
						   {
							  $_Namespaces['webhook']->username($value);
						   }
						   else
						   {
							  $execute = False;
							  throw new \Exception('[Discord-API]: Invalid BOT Username: \'@\',\'#\',\':\',\'`\', whitespace');
						   }
						}
						else
						{
						    $execute = False;
						    throw new \Exception('[Discord-API]: Invalid `bot.username` argument\ length limit: ' .strlen($value). '/32');
						}
					 }
					 break;
		       case 'bot_avatar':
			         if($value != NULL)
					 {
						if(preg_match('/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/', $value) && file_get_contents($value))
						{
						   $_Namespaces['webhook']->avatar($value);
						}
						else
						{
						   $execute = False;
						   throw new \Exception('[Discord-API]: There must be a valid `bot.avatar` argument...');
						}
					 }
					 break;	
		       case 'tts':
			         $_Namespaces['webhook']->tts(boolval(($value == 'True' || $value == 'true' || $value == 'TRUE' || $value == 1)));
					 break;					 
			}
		 }
	  }
	  else
	  {
         $execute = False;
		 throw new \Exception('[Discord-API]: There must be a valid `main` Configuration array set...');  
	  }
	  if(isset($data['general']) && $data['general'] != NULL && is_array($data['general']))
	  {
	     foreach($data['general'] as $key => $value)
		 {
	        if($key == end($data['general']))
		    {
			   break;
			}			 
			switch($key)
			{	
		       case 'description':
			         if($value != NULL)
					 {
						if(strlen($value) <= 2048)
						{
						   $_Namespaces['embed']->description($value);
						}
						else
						{
						   $execute = False;
						   throw new \Exception('[Discord-API]: `embed.description` argument\'s length limit excceded: ' .strlen($value). '/2048');
						}						
		                $_embedC++;
					 }
					 break;
		       case 'title':
			         if($value != NULL)
					 {
						if(strlen($value) <= 256)
						{						 			   
						   $_Namespaces['embed']->title($value);
					    }
					    else
					    {
					       $execute = False;
					       throw new \Exception('[Discord-API]: `embed.title` argument\'s length limit excceded: ' .strlen($value). '/256');
					    }						
		                $_embedC++;
					 }
					 break;
		       case 'color':
			         if($value != NULL)
					 {
						if(strcmp($value, 'auto') == False || strcmp($value, 'default') == False)
						{
						   $_Namespaces['embed']->color($_Namespaces['embed']->COLOR_['cyan']);
						}
						else
						{
						   $_Namespaces['embed']->color($value[0] == '#' ? hexdec(str_replace('#', '', $value)) : $value);
						}
		                $_embedC++;
					 }
					 break;		       
		       case 'thumbnail':
			         if($value != NULL)
					 {
						if(preg_match('/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/', $value) && file_get_contents($value))
						{
						   $_Namespaces['embed']->thumbnail($value);
						}
						else
						{
						   $execute = False;
						   throw new \Exception('[Discord-API]: There must be a valid `embed.thumbnail` argument...');
						}						
		                $_embedC++;
					 }
					 break;
			   case 'image':
			         if($value != NULL)
					 {
						if(preg_match('/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/', $value) && file_get_contents($value))
						{
						   $_Namespaces['embed']->image($value);
						}
						else
						{
						   $execute = False;
						   throw new \Exception('[Discord-API]: There must be a valid `embed.image` argument...');
						}
		                $_embedC++;
					 }
					 break;
			   case 'timestamp':
			         if($value != NULL)
					 {
						if(strcmp($value, 'auto') == False || strcmp($value, 'default') == False)
						{
						   $timezone = new DateTime("now", new DateTimeZone(date_default_timezone_get()));
						   $_Namespaces['embed']->timestamp(date("Y-m-d\TH:i:s." .round(microtime(True) * 1000). "\\" . $timezone->format('P')));
						}
						else
                        {
						   if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]+[\-\+][0-9]{2}:[0-9]{2}/", $value))
						   {
							  $_Namespaces['embed']->timestamp($value);
						   }
						   else
						   {
							  $execute = False;
							  throw new \Exception('[Discord-API]: There must be a valid `ISO-8601 timestamp` argument... (Valid Timestamp Format: YYYY-MM-DDTHH:MM:SS.MSSZ)');							   
						   }
						}
						$_embedC++;
					 }
					 break;
		       case 'message':
					 if($value != NULL)
	  			     {
						if(strlen($value) <= 2000)
						{
							$_Namespaces['webhook']->message($value);
						}
						else
						{
							$execute = False;
							throw new \Exception('[Discord-API]: `webhook.message` argument\'s length limit excceded: ' .strlen($value). '/2000');
						}
	     		     }
	     		     else
	     		     {
	        		   if($_embedC <= 0)
		    		   {
			   		      $execute = False;
		       		      throw new \Exception('[Discord-API]: There must be a valid `Message`, `Embed` Arguments...(it is possible to use both of them at the same time)');
		    		   }
	     		     }
					 break;
			   case 'file':
			         if($value != NULL)
					 {
						if(isset($data['general']['file']['url']) && file_get_contents($data['general']['file']['url']))
						{
						   $_Namespaces['webhook']->file(new Discord\Attachment($data['general']['file']['url'], (isset($data['general']['file']['name']) ? $data['general']['file']['name'] : NULL)));
						}
					 }
					 break;			   
			}			
		 }
	  }	  
	  if(isset($data['author']) && $data['author'] != NULL && is_array($data['author']))
	  {
         $_Namespaces['embed']->author($data['author']['name'], (isset($data['author']['url']) ? $data['author']['url'] : NULL), (isset($data['author']['avatar_url']) ? (preg_match('/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/', $data['author']['avatar_url']) && file_get_contents($data['author']['avatar_url']) ? $data['author']['avatar_url'] : NULL) : NULL));
		 $_embedC++;
	  }
	  if(isset($data['footer']) && $data['footer'] != NULL && is_array($data['footer']))
	  {
		 if(isset($data['footer']['image_url']))
		 {
			if(preg_match('/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/', $data['footer']['image_url']) && file_get_contents($data['footer']['image_url']))
			{
			   $img_url = $data['footer']['image_url'];
			}
			else
			{
			   $img_url = NULL;
			   $execute = False;
			   throw new \Exception('[Discord-API]: There must be a valid `embed.footer.image_url` argument...');
			}
		 }
		 else
		 {
		    $img_url = NULL;
		 }
		 if(strlen($data['footer']['text']) <= 2048)
		 {
			$_Namespaces['embed']->footer($data['footer']['text'], $img_url);
		 }
		 else
		 {
		    $execute = False;
	        throw new \Exception('[Discord-API]: `embed.footer.text` argument\'s length limit excceded: ' .strlen($data['footer']['text']). '/2048');
		 }
		 $_embedC++;
	  }
	  if(isset($data['fields']) && $data['fields'] != NULL && is_array($data['fields']) && count($data['fields']) >= 1)
	  {
         if(count($data['fields']) <= 25)
	     {		  
            for($f = 0; $f < count($data['fields']); $f++)
            {
			   if(strlen($data['fields'][$f]['name']) <= 256)
               {
				  if(strlen($data['fields'][$f]['value']) <= 1024)
				  {
					 $_Namespaces['embed']->field($data['fields'][$f]['name'], $data['fields'][$f]['value'], (isset($data['fields'][$f]['inline']) ? boolval($data['fields'][$f]['inline']) : False));
				  }
				  else
				  {
					 $execute = False;
					 throw new \Exception('[Discord-API]: `embed.field.value#' .$f. '` argument\'s length limit excceded: ' .strlen($data['fields'][$f]['value']). '/1024');					  
				  }
			   }
			   else
			   {
				  $execute = False;
				  throw new \Exception('[Discord-API]: `embed.field.name#' .$f. '` argument\'s length limit excceded: ' .strlen($data['fields'][$f]['name']). '/256');
			   }
            }
         }
	     else
       	 {
	        throw new \Exception('[Discord-API]: Discord\'s Embed Fields Limit exceeded: ' .count($data['fields']). '/25');
	     }		 
         $_embedC++;
      }
	  if($_embedC >= 1)
	  {
		 $_Namespaces['webhook']->embed($_Namespaces['embed']);
	  }
	  if($execute == True)
	  {
         $_Namespaces['webhook']->send();
      }
   }
   
   function FormatData($data, $footer = '') // Thanks alot to Pastebin Community for thier Code Embbeding Template.
   {
	  if(strlen($data) >= 1)
	  {
		 $_Stylesheets = "body { font-family: monospace; padding: 0; margin: 0; }div.embedPastebin { padding: 0; color: #000; margin: 0; font-family: monospace; background: #F7F7F7; border: 1px solid ddd; border-radius:3px; }html.embedPBBody div.embedPastebin { border: none; }div.embedPastebin div.embedFooter { background: #F7F7F7; color: #333; font-size: 100%; padding: 6px 12px; border-bottom: 1px solid #ddd; text-transform:uppercase; }div.embedPastebin div.embedFooter a, div.embedPastebin div.embedFooter a:visited { color: #336699; text-decoration:none; }div.embedPastebin div.embedFooter a:hover { color: red; }.noLines ol { list-style-type: none; padding-left: 0.5em; }.embedPastebin{background-color:#F8F8F8;border:1px solid #ddd;font-size:12px;overflow:auto;margin: 0 0 0 0;padding:0 0 0 0;line-height:21px;}.embedPastebin div { line-height:21px; font-family:Consolas, Menlo, Monaco, Lucida Console,'Bitstream Vera Sans Mono','Courier','monospace','Courier New';} ol { margin:0; padding: 0 0 0 55px} ol li { border:0; margin:0;padding:0; }.text  {color:#ACACAC;max-height:200px;overflow-y:scroll;}.text .imp {font-weight: bold; color: red;}.text .ln-xtra, .text li.ln-xtra, .text div.ln-xtra {background:#FFFF88;}.text span.xtra { display:block; }.text .ln {width:1px;text-align:right;margin:0;padding:0 2px;vertical-align:top;}";
		 $_Footer = (strlen($footer) >= 1 && $footer != NULL ? $footer : 'Sent JSON Data Preview');
		 $_Elements = "<div class=\"embedPastebin\"><div class=\"embedFooter\">" .$_Footer. "</div><ol class=\"text\">";
		 $_Output = explode('\n', $data);
		 for($l = 0; $l < count($_Output); $l++)
		 {
			$_Output[$l] = str_replace('\r', ' ', $_Output[$l]);
			$_Stylesheets .= 'li.ln-xtra .de' .$l. ' {background:#F8F8CE;}.text .de' .$l. ' {-moz-user-select: text;-khtml-user-select: text;-webkit-user-select: text;-ms-user-select: text;user-select: text;margin:0; padding: 0 8px; background:none; vertical-align:top;color:#000;border-left: 1px solid #ddd; margin: 0 0 0 -7px; position: relative; background: #ffffff;}.embedPastebin ol li.li' .$l. ' { margin: 0; }.text li, .text .li' .$l. ' {-moz-user-select: -moz-none;-khtml-user-select: none;-webkit-user-select: none;-ms-user-select: none;user-select: none;}';
			$_Elements .= '<li class="li' .$l. '"><div class="de' .$l. '">' .$_Output[$l]. '</div></li>';
		 }
		 $_Elements .= '</ol></div>';
         $_Output = '<style>' .$_Stylesheets. '</style>' .$_Elements; 
	  }
	  else
	  {
	     $_Output = '<p>Invalid data Length</p>';
	  }
	  return $_Output;
   }

   if($Data = json_decode(file_get_contents("php://input"), True))
   {
	  switch (json_last_error()) 
	  {
		 case JSON_ERROR_NONE:
		    DS_SendBOTMessage($Data);
            /* Some kind of Code Debugging... */
			echo FormatData(json_encode($Data, JSON_PRETTY_PRINT), ('Data sent successfully in <b>' .((microtime(True) - $_START_TIME)*1000). ' ms</b>'));   
			break;
   		 case JSON_ERROR_DEPTH:
    		$json_err = 'Maximum stack depth exceeded';
   			break;
    	 case JSON_ERROR_STATE_MISMATCH:
    		$json_err = 'Underflow or the modes mismatch';
    		break;
   		 case JSON_ERROR_CTRL_CHAR:
    		$json_err = 'Unexpected control character found';
   			break;
    	 case JSON_ERROR_SYNTAX:
       		$json_err = 'Syntax error, malformed JSON';
   			break;
   		 case JSON_ERROR_UTF8:
            $json_err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
        	break;
         default:
           	$json_err = 'Unknown error';
        	break;
      }
	  if(isset($json_err))
	  {
	     throw new \Exception('[Discord-API]: The server returned an error while fetching the requested data: ' . $json_err);	  
	  }
   }
   else
   {
	  if(file_get_contents("php://input"))
	  {
         throw new \Exception('[Discord-API]: Invalid Data Type... (Valid Type: JSON)');    
	  }
	  else
	  {
		 throw new \Exception('[Discord-API]: Data Parameter must be set. (with a valid type: JSON)');
	  }
   }
   /*
      @04-04-2018 10:39 PM
	  * it token 435.46104431152 ms = 0.43546104431152 s = 0.01 min to execute successfully...
   */
?>